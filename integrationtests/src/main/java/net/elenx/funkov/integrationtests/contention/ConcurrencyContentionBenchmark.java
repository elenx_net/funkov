package net.elenx.funkov.integrationtests.contention;

import net.elenx.funkov.common.OS;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Timeout;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.VarHandle;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/*
    Benchmark                                                 Mode  Cnt       Score       Error   Units
    ConcurrencyContentionBenchmark.atomicBoolean             thrpt    5  278484.428 ±  7433.947  ops/ms
    ConcurrencyContentionBenchmark.atomicFieldUpdater        thrpt    5  298112.201 ±  4824.366  ops/ms
    ConcurrencyContentionBenchmark.readWriteLock             thrpt    5   86309.037 ±  2178.484  ops/ms
    ConcurrencyContentionBenchmark.reentrantLock             thrpt    5   87532.244 ±  1114.310  ops/ms
    ConcurrencyContentionBenchmark.synchronizeed             thrpt    5   97100.726 ±  3557.496  ops/ms
    ConcurrencyContentionBenchmark.unsafeGetVolatileBoolean  thrpt    5  272842.917 ± 21372.611  ops/ms
    ConcurrencyContentionBenchmark.unsafeGetVolatileByte     thrpt    5  271071.005 ±  1193.678  ops/ms
    ConcurrencyContentionBenchmark.varHandleAcquire          thrpt    5  288421.777 ±  1147.096  ops/ms
    ConcurrencyContentionBenchmark.varHandleOpaque           thrpt    5  294637.758 ± 12456.305  ops/ms
    ConcurrencyContentionBenchmark.varHandleVolatile         thrpt    5  294840.128 ±  6721.305  ops/ms
    ConcurrencyContentionBenchmark.volatilee                 thrpt    5  292842.199 ±  2603.206  ops/ms
    ConcurrencyContentionBenchmark.withoutSynchronization    thrpt    5  290399.601 ± 12817.372  ops/ms
*/

@Fork(value = 1)
@Warmup(iterations = 5)
@Measurement(iterations = 5)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@BenchmarkMode({Mode.Throughput})
@State(Scope.Benchmark)
@Timeout(time = 1)
public class ConcurrencyContentionBenchmark
{
    private static final Object lock = new Object();
    private static final ReentrantLock reentrantLock = new ReentrantLock();
    private static final ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private static final AtomicBoolean atomicBoolean = new AtomicBoolean(true);
    public boolean running = true;
    public volatile boolean runningVolatile = true;

    @Benchmark
    public void synchronizeed(Blackhole blackhole)
    {
        synchronized (lock)
        {
            if (running)
                blackhole.consume(running);
        }
    }

    @Benchmark
    public void reentrantLock(Blackhole blackhole)
    {
        try
        {
            reentrantLock.lock();
            if (running)
                blackhole.consume(running);
        } finally
        {
            reentrantLock.unlock();
        }
    }

    @Benchmark
    public void readWriteLock(Blackhole blackhole)
    {
        try
        {
            readWriteLock.readLock().lock();
            if (running)
                blackhole.consume(running);
        } finally
        {
            readWriteLock.readLock().unlock();
        }
    }

    @Benchmark
    public void volatilee(Blackhole blackhole)
    {
        if (runningVolatile)
            blackhole.consume(running);
    }

    @Benchmark
    public void atomicBoolean(Blackhole blackhole)
    {
        if (atomicBoolean.get())
            blackhole.consume(running);
    }

    private static final VarHandle VALUE;
    private static final long isRunningBooleanAddress = OS.memory().allocate(1);
    private static final long isRunningByteAddress = OS.memory().allocate(1);

    static
    {
        try
        {
            VALUE = MethodHandles
                    .lookup()
                    .in(ConcurrencyContentionBenchmark.class)
                    .findVarHandle(ConcurrencyContentionBenchmark.class, "running", boolean.class);
        } catch (ReflectiveOperationException e)
        {
            throw new ExceptionInInitializerError(e);
        }
        OS.memory().putBooleanVolatile(isRunningBooleanAddress, true);
        OS.memory().putByteVolatile(isRunningByteAddress, (byte) 1);

    }

    @Benchmark
    public void varHandleOpaque(Blackhole blackhole)
    {
        if ((boolean) VALUE.getOpaque(this))
            blackhole.consume(running);
    }

    @Benchmark
    public void varHandleAcquire(Blackhole blackhole)
    {
        if ((boolean) VALUE.getAcquire(this))
            blackhole.consume(running);
    }

    @Benchmark
    public void varHandleVolatile(Blackhole blackhole)
    {
        if ((boolean) VALUE.getVolatile(this))
            blackhole.consume(running);
    }

    @Benchmark
    public void unsafeGetVolatileBoolean(Blackhole blackhole)
    {
        if (OS.memory().getBooleanVolatile(isRunningBooleanAddress))
        {
            blackhole.consume(isRunningBooleanAddress);
        }
    }

    @Benchmark
    public void unsafeGetVolatileByte(Blackhole blackhole)
    {
        if (OS.memory().getByteVolatile(isRunningByteAddress) == 1)
        {
            blackhole.consume(isRunningBooleanAddress);
        }
    }

    volatile int run = 1;
    private static final AtomicIntegerFieldUpdater<ConcurrencyContentionBenchmark> runFieldUpdater =
            AtomicIntegerFieldUpdater.newUpdater(ConcurrencyContentionBenchmark.class, "run");

    @Benchmark
    public void atomicFieldUpdater(Blackhole blackhole)
    {
        if (runFieldUpdater.get(this) == 1)
            blackhole.consume(running);
    }

    @Benchmark
    public void withoutSynchronization(Blackhole blackhole)
    {
        if (running)
        {
            blackhole.consume(running);
        }
    }


    public static void main(String[] args) throws RunnerException
    {

        Options options = new OptionsBuilder()
                .include(ConcurrencyContentionBenchmark.class.getSimpleName())
                .jvmArgs("--enable-preview"/*, "-XX:+UnlockDiagnosticVMOptions", "-XX:+PrintAssembly", "-XX:+LogCompilation", "-XX:PrintAssemblyOptions=amd64"*/)
                .build();
        new Runner(options).run();
    }
}
