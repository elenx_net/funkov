package net.elenx.funkov.integrationtests.handshake;

import net.openhft.chronicle.bytes.Bytes;
import net.openhft.chronicle.bytes.VanillaBytes;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Timeout;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.nio.ByteBuffer;
import java.util.Random;
import java.util.concurrent.TimeUnit;


/*
Benchmark                                            (bytesToWrite)  Mode  Cnt    Score    Error  Units
HandshakeBenchmark.writeByteArray                                 6  avgt    5    7.665 ±  0.581  ns/op
HandshakeBenchmark.writeByteArray                               509  avgt    5   15.743 ±  0.140  ns/op
HandshakeBenchmark.writeByteArray                              1024  avgt    5   22.222 ±  0.085  ns/op
HandshakeBenchmark.writeByteBufferHeapMemCopy                     6  avgt    5   12.277 ±  0.218  ns/op
HandshakeBenchmark.writeByteBufferHeapMemCopy                   509  avgt    5  331.552 ± 25.817  ns/op
HandshakeBenchmark.writeByteBufferHeapMemCopy                  1024  avgt    5  609.873 ±  4.260  ns/op
HandshakeBenchmark.writeByteBufferHeapSequential                  6  avgt    5   12.007 ±  0.579  ns/op
HandshakeBenchmark.writeByteBufferHeapSequential                509  avgt    5  319.322 ± 11.348  ns/op
HandshakeBenchmark.writeByteBufferHeapSequential               1024  avgt    5  656.337 ± 18.072  ns/op
HandshakeBenchmark.writeByteBufferOffHeapMemCopy                  6  avgt    5    7.024 ±  0.065  ns/op
HandshakeBenchmark.writeByteBufferOffHeapMemCopy                509  avgt    5   37.684 ±  0.709  ns/op
HandshakeBenchmark.writeByteBufferOffHeapMemCopy               1024  avgt    5   74.019 ±  0.648  ns/op
HandshakeBenchmark.writeByteBufferOffHeapSequential               6  avgt    5    6.848 ±  0.038  ns/op
HandshakeBenchmark.writeByteBufferOffHeapSequential             509  avgt    5   38.981 ±  0.894  ns/op
HandshakeBenchmark.writeByteBufferOffHeapSequential            1024  avgt    5   74.425 ±  4.397  ns/op
HandshakeBenchmark.writeString                                    6  avgt    5   21.257 ±  0.212  ns/op
HandshakeBenchmark.writeString                                  509  avgt    5  429.120 ± 13.352  ns/op
HandshakeBenchmark.writeString                                 1024  avgt    5  851.410 ± 30.074  ns/op
*/

// writeByteArray also uses memcopy

@Fork(value = 1)
@Warmup(iterations = 6, time = 12)
@Measurement(iterations = 5, time = 10)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@BenchmarkMode({Mode.AverageTime})
@State(Scope.Benchmark)
@Timeout(time = 1)
public class WritingStringsToOffHeapByteBufferBenchmark
{

    @Benchmark
    public void writeByteBufferOffHeapMemCopy(HandshakeBytes handshakeBytes)
    {
        handshakeBytes.bytesContainer.writePosition(0);
        handshakeBytes.offHeapByteBuffer.readPosition(0);
        handshakeBytes.bytesContainer.write(handshakeBytes.offHeapByteBuffer, 0, handshakeBytes.offHeapByteBuffer.readLimit());
    }

    @Benchmark
    public void writeByteBufferOffHeapSequential(HandshakeBytes handshakeBytes)
    {
        handshakeBytes.bytesContainer.writePosition(0);
        handshakeBytes.offHeapByteBuffer.readPosition(0);
        handshakeBytes.bytesContainer.write(handshakeBytes.offHeapByteBuffer);
    }

    @Benchmark
    public void writeByteBufferHeapMemCopy(HandshakeBytes handshakeBytes)
    {
        handshakeBytes.bytesContainer.writePosition(0);
        handshakeBytes.heapByteBuffer.readPosition(0);
        handshakeBytes.bytesContainer.write(handshakeBytes.heapByteBuffer, 0, handshakeBytes.heapByteBuffer.readLimit());
    }

    @Benchmark
    public void writeByteBufferHeapSequential(HandshakeBytes handshakeBytes)
    {
        handshakeBytes.bytesContainer.writePosition(0);
        handshakeBytes.heapByteBuffer.readPosition(0);
        handshakeBytes.bytesContainer.write(handshakeBytes.heapByteBuffer);
    }

    @Benchmark
    public void writeByteArray(HandshakeBytes handshakeBytes)
    {
        handshakeBytes.bytesContainer.writePosition(0);
        handshakeBytes.bytesContainer.write(handshakeBytes.byteArray);
    }

    @Benchmark
    public void writeString(HandshakeBytes handshakeBytes)
    {
        handshakeBytes.bytesContainer.writePosition(0);
        handshakeBytes.bytesContainer.write(handshakeBytes.string);
    }


    @State(Scope.Benchmark)
    public static class HandshakeBytes
    {
        private static final Random r = new Random();
        public final VanillaBytes<?> bytesContainer = Bytes.allocateDirect(2048);

        @Param({"6", "509", "1024"})
        public int bytesToWrite;

        String string;
        byte[] byteArray;
        Bytes<ByteBuffer> heapByteBuffer;
        Bytes<ByteBuffer> offHeapByteBuffer;

        @Setup(Level.Trial)
        public void setUp()
        {
            System.out.println("TRIAL " + bytesToWrite);
            char[] chars = new char[bytesToWrite];
            for (int i = 0; i < bytesToWrite; i++)
                chars[i] = (char) (r.nextInt(26) + 'a');

            string = new String(chars);
            byteArray = string.getBytes();
            heapByteBuffer = Bytes.elasticHeapByteBuffer(bytesToWrite).append(string);
            offHeapByteBuffer = Bytes.elasticByteBuffer(bytesToWrite).append(string);
        }
    }


    public static void main(String[] args) throws RunnerException
    {

        Options options = new OptionsBuilder()
                .include(WritingStringsToOffHeapByteBufferBenchmark.class.getSimpleName())
                //.jvmArgs("--enable-preview", "-XX:+UnlockDiagnosticVMOptions", "-XX:+PrintAssembly", "-XX:+LogCompilation", "-XX:PrintAssemblyOptions=amd64")
                .build();
        new Runner(options).run();
    }
}
