package net.elenx.funkov.integrationtests.bytespool;

import net.elenx.funkov.common.OS;
import net.elenx.funkov.common.bytes.BytesPool;
import net.elenx.funkov.common.bytes.BytesPoolFactory;
import net.openhft.chronicle.bytes.Bytes;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Timeout;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.nio.ByteBuffer;
import java.util.concurrent.TimeUnit;

/*
   Benchmark                              (bytesPool)  Mode  Cnt     Score    Error  Units
   BytesPoolBenchmark.bytesPoolBenchmark    reference  avgt    5  1730.486 ± 48.337  ms/op
   BytesPoolBenchmark.bytesPoolBenchmark      address  avgt    5   628.585 ± 61.500  ms/op
*/

@Fork(value = 1)
@Warmup(iterations = 5, time = 10)
@Measurement(iterations = 5, time = 10)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@BenchmarkMode({Mode.AverageTime})
@State(Scope.Benchmark)
@Timeout(time = 1)
public class BytesPoolBenchmark
{
    private static final int maxPoolSize = 40_000;
    private static final int maxBufferSize = OS.tcpBufferSize() + OS.memory().pageSize();

    private static final BytesPool referenceBytesPool = BytesPoolFactory.referenceBytesPool(maxPoolSize, maxBufferSize);
    private static BytesPool pool;

    @Param({"reference"})
    String bytesPool;

    @Setup(Level.Trial)
    public void setup()
    {
        if (bytesPool.equals("reference"))
            pool = referenceBytesPool;
    }

    @Benchmark
    public void bytesPoolBenchmark(Blackhole blackhole)
    {
        Bytes<ByteBuffer>[] buffers = new Bytes[maxPoolSize];
        for (int i = 0; i < maxPoolSize; i++)
        {
            buffers[i] = pool.leaseBuffer();
        }

        for (int i = 0; i < maxPoolSize; i++)
        {
            pool.releaseBuffer(buffers[i]);
            buffers[i] = null;
        }

        for (int i = 0; i < maxPoolSize; i++)
        {
            var buffer = pool.leaseBuffer();
            buffer.releaseLast();
            blackhole.consume(buffer);
        }
    }

    public static void main(String[] args) throws RunnerException
    {

        Options options = new OptionsBuilder()
                .include(BytesPoolBenchmark.class.getSimpleName())
                .jvmArgs("-XX:MaxDirectMemorySize=20G", "--enable-preview")
                .build();
        new Runner(options).run();
    }
}
