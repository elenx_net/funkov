package net.elenx.funkov.integrationtests.server;

import net.elenx.funkov.protocol.websocket.WebSocketConnectionListener;
import net.elenx.funkov.protocol.websocket.WebsocketProtocol;
import net.elenx.funkov.server.Server;
import net.elenx.funkov.server.configuration.FunkovServerConfiguration;
import net.elenx.funkov.server.warmup.WebSocketServerWarmup;

public class ServerRunner
{
    private static final int DEFAULT_WARMUP_REQUESTS = 500;

    public static void main(String[] args)
    {
        int port = 5555;
        int threads = 1;

        int warmupIterations = args.length == 0 ? DEFAULT_WARMUP_REQUESTS : Integer.parseInt(args[0]);
        boolean closeAfterWarmup = args.length >= 2 && Boolean.parseBoolean(args[1]);

        FunkovServerConfiguration configuration = FunkovServerConfiguration.builder()
                .workerThreads(threads)
                .warmup(new WebSocketServerWarmup(warmupIterations, closeAfterWarmup))
                .port(port)
                .build();

        Server server = Server.server(configuration, new WebsocketProtocol(new MockedWebSocketConnectionListener()));
        server.start();
    }


    public static class MockedWebSocketConnectionListener implements WebSocketConnectionListener
    {

    }
}
