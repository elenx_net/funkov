package net.elenx.funkov.integrationtests.bytespool;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Timeout;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Field;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.concurrent.TimeUnit;

/*
  Benchmark                                     Mode  Cnt     Score     Error   Units
  ReflectionBenchmark.methodHandleInvoke       thrpt    5  2141.648 ±   2.046  ops/us
  ReflectionBenchmark.methodHandleInvokeExact  thrpt    5  2193.995 ± 119.886  ops/us
  ReflectionBenchmark.plainReflection          thrpt    5   257.507 ±   5.611  ops/us
 */

@Fork(value = 1)
@Warmup(iterations = 5, time = 10)
@Measurement(iterations = 5, time = 10)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
@BenchmarkMode({Mode.Throughput})
@State(Scope.Benchmark)
@Timeout(time = 1)
public class ReflectionBenchmark
{
    private static final Buffer bb = ByteBuffer.allocateDirect(0).order(ByteOrder.nativeOrder());

    private static final Field address, capacity;

    static
    {
        try
        {
            address = Buffer.class.getDeclaredField("address");
            address.setAccessible(true);
            capacity = Buffer.class.getDeclaredField("capacity");
            capacity.setAccessible(true);
        } catch (NoSuchFieldException e)
        {
            throw new AssertionError(e);
        }
    }

    @Benchmark
    public void plainReflection() throws IllegalAccessException
    {
        address.setLong(bb, 5000000L);
        capacity.setInt(bb, 500);
    }

    private static final MethodHandles.Lookup lookup = MethodHandles.lookup();
    private static final MethodHandle addressSetter, capacitySetter;

    static
    {
        try
        {
            addressSetter = lookup.unreflectSetter(address);
            capacitySetter = lookup.unreflectSetter(capacity);
        } catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    @Benchmark
    public void methodHandleInvokeExact() throws Throwable
    {
        addressSetter.invokeExact(bb, 5000000L);
        capacitySetter.invokeExact(bb, 500);
    }

    @Benchmark
    public void methodHandleInvoke() throws Throwable
    {
        addressSetter.invoke(bb, 5000000L);
        capacitySetter.invoke(bb, 500);
    }


    public static void main(String[] args) throws RunnerException
    {

        Options options = new OptionsBuilder()
                .include(ReflectionBenchmark.class.getSimpleName())
                .build();
        new Runner(options).run();
    }
}
