package net.elenx.funkov.integrationtests.bytes;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Timeout;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/*
Benchmark                                          (bytesToWrite)  Mode  Cnt   Score   Error  Units
CopyingByteArraysBenchmark.copyBySingleByteInLoop               6  avgt    5   3.631 ± 0.569  ns/op
CopyingByteArraysBenchmark.copyBySingleByteInLoop              96  avgt    5  15.139 ± 0.491  ns/op
CopyingByteArraysBenchmark.copyBySingleByteInLoop             509  avgt    5  16.826 ± 0.210  ns/op
CopyingByteArraysBenchmark.copyBySingleByteInLoop            1024  avgt    5  22.589 ± 0.486  ns/op
CopyingByteArraysBenchmark.copyBySingleByteInLoop            4096  avgt    5  43.300 ± 0.343  ns/op
CopyingByteArraysBenchmark.copyBySystemArrayCopy                6  avgt    5   2.773 ± 0.194  ns/op
CopyingByteArraysBenchmark.copyBySystemArrayCopy               96  avgt    5   3.371 ± 0.143  ns/op
CopyingByteArraysBenchmark.copyBySystemArrayCopy              509  avgt    5   8.520 ± 0.099  ns/op
CopyingByteArraysBenchmark.copyBySystemArrayCopy             1024  avgt    5  14.803 ± 0.246  ns/op
CopyingByteArraysBenchmark.copyBySystemArrayCopy             4096  avgt    5  63.928 ± 3.500  ns/op
*/


@Fork(value = 1)
@Warmup(iterations = 6, time = 12)
@Measurement(iterations = 5, time = 10)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@BenchmarkMode({Mode.AverageTime})
@State(Scope.Benchmark)
@Timeout(time = 1)
public class CopyingByteArraysBenchmark
{

    @Benchmark
    public void copyBySystemArrayCopy(Bytes bytes)
    {
        System.arraycopy(bytes.byteArray, 0, bytes.BYTES_CONTAINER, 0, bytes.byteArray.length);
    }

    @Benchmark
    public void copyBySingleByteInLoop(Bytes bytes)
    {
        for (int i = 0; i < bytes.byteArray.length; i++)
            bytes.BYTES_CONTAINER[i] = bytes.byteArray[i];
    }

    @State(Scope.Benchmark)
    public static class Bytes
    {
        public final byte[] BYTES_CONTAINER = new byte[4096];

        @Param({"6", "96", "509", "1024", "4096"})
        public int bytesToWrite;

        byte[] byteArray;

        @Setup(Level.Trial)
        public void setUp()
        {
            byteArray = new byte[bytesToWrite];
            new Random().nextBytes(byteArray);
        }
    }

    public static void main(String[] args) throws RunnerException
    {

        Options options = new OptionsBuilder()
                .include(CopyingByteArraysBenchmark.class.getSimpleName())
                //.jvmArgs("--enable-preview", "-XX:+UnlockDiagnosticVMOptions", "-XX:+PrintAssembly", "-XX:+LogCompilation", "-XX:PrintAssemblyOptions=amd64")
                .build();
        new Runner(options).run();
    }
}
