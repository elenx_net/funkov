package net.elenx.funkov.integrationtests.websocket.handshake

import net.elenx.funkov.client.WebSocketClient
import net.elenx.funkov.client.WebSocketClientFactory
import net.elenx.funkov.client.WebSocketClientListener
import net.elenx.funkov.common.handler.BooleanHandler
import net.elenx.funkov.common.TestExpectedException
import net.elenx.funkov.common.handler.Handler
import net.elenx.funkov.common.handler.Handlers
import net.elenx.funkov.model.Handshake
import net.elenx.funkov.protocol.websocket.WebSocketConnectionListener
import net.elenx.funkov.protocol.websocket.WebsocketProtocol
import net.elenx.funkov.protocol.websocket.mock.MockWebSocketConnectionListener
import net.elenx.funkov.server.Server
import net.elenx.funkov.server.configuration.FunkovServerConfiguration
import net.elenx.funkov.serverapi.ServerListener
import spock.lang.Specification

class WebSocketConnectionListenerTest extends Specification
{
    private static final port = 5555
    private static final FunkovServerConfiguration CONFIGURATION = FunkovServerConfiguration.builder()
            .port(port)
            .build()
    URI SERVER_URI = new URI("ws://127.0.0.1:${port}")
    
    def "should invoke onStartServer() method when server is started"()
    {
        given:
            BooleanHandler handler = Handlers.handler()
            WebSocketConnectionListener connectionListener = MockWebSocketConnectionListener
                    .builder()
                    .onStartServer(() -> handler.invoke())
                    .build()
        
        when:
            ServerListener listener = listener(connectionListener)
            Server server = Server.server(CONFIGURATION, listener)
            server.startOnNewThread()
        
        then:
            handler.invoked()
        
        cleanup:
            server.close()
    }
    
    def "should invoke onHandshake() method on client handshake"()
    {
        given:
            Handler<Handshake> handshakeHandler = Handlers.handler(Handshake.class)
            BooleanHandler handler = Handlers.handler()
            WebSocketClient client = WebSocketClientFactory.nettyClient(SERVER_URI.toString())
            WebSocketConnectionListener connectionListener = MockWebSocketConnectionListener
                    .builder()
                    .onHandshake((socket, handshake) -> handler.invoke())
                    .build()
        
        when:
            ServerListener listener = listener(connectionListener)
            Server server = Server.server(CONFIGURATION, listener)
            server.startOnNewThread()
            client.connect(createListener(handshakeHandler))
        then:
            handler.invoked()
        cleanup:
            server.close()
            client.close()
        
    }
    
    def "should invoke onConnectionError() method when error occurs while handling handshake "()
    {
        given:
            Handler<Handshake> handshakeHandler = Handlers.handler(Handshake.class)
            BooleanHandler handler = Handlers.handler()
            WebSocketClient client = WebSocketClientFactory.nettyClient(SERVER_URI.toString())
            WebSocketConnectionListener connectionListener = MockWebSocketConnectionListener
                    .builder()
                    .onHandshake((socket, handshake) -> { throw new TestExpectedException() })
                    .onConnectionError((exc) -> handler.invoke())
                    .build()
        
        when:
            ServerListener listener = listener(connectionListener)
            Server server = Server.server(CONFIGURATION, listener)
            server.startOnNewThread()
            client.connect(createListener(handshakeHandler))
        then:
            handler.invoked()
        cleanup:
            server.close()
            client.close()
    }
    
    def "should not invoke onConnectionError() method when error occurs during server start"()
    {
        given:
            WebSocketConnectionListener connectionListener = MockWebSocketConnectionListener
                    .builder()
                    .onStartServer(() -> { throw new TestExpectedException() })
                    .build()
        
        when:
            ServerListener listener = listener(connectionListener)
            Server server = Server.server(CONFIGURATION, listener)
            server.startOnNewThread()
        
        then:
            0 * connectionListener.onConnectionError(_)
        
        cleanup:
            server.close()
    }
    
    
    private static ServerListener listener(WebSocketConnectionListener connectionListener)
    {
        return new WebsocketProtocol(connectionListener)
    }
    
    private static WebSocketClientListener createListener(Handler<Handshake> handshakeHandler)
    {
        return new WebSocketClientListener() {
            @Override
            void onConnect(Handshake handshake)
            {
                handshakeHandler.invoke(handshake)
            }
        }
    }
}
