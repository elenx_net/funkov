package net.elenx.funkov.integrationtests.websocket.handshake

import net.elenx.funkov.client.WebSocketClient
import net.elenx.funkov.client.WebSocketClientFactory
import net.elenx.funkov.client.WebSocketClientListener
import net.elenx.funkov.common.handler.Handler
import net.elenx.funkov.common.handler.Handlers
import net.elenx.funkov.model.CloseFrame
import net.elenx.funkov.protocol.websocket.WebSocketConnectionListener
import net.elenx.funkov.protocol.websocket.WebsocketProtocol
import net.elenx.funkov.protocol.websocket.mock.MockWebSocketConnectionListener
import net.elenx.funkov.server.Server
import net.elenx.funkov.server.configuration.FunkovServerConfiguration
import net.elenx.funkov.serverapi.ServerListener
import spock.lang.Specification

class CloseFrameTest extends Specification
{
    private static final port = 5555
    private static final FunkovServerConfiguration CONFIGURATION = FunkovServerConfiguration.builder()
            .port(port)
            .build()
    URI SERVER_URI = new URI("ws://127.0.0.1:${port}")
    
    def "should send close frame and close connection after receiving close frame from client"()
    {
        given:
            Handler<CloseFrame> closeFrameHandler = Handlers.handler(CloseFrame.class)
            WebSocketClient client = WebSocketClientFactory.nettyClient(SERVER_URI.toString())
            WebSocketConnectionListener connectionListener = MockWebSocketConnectionListener
                    .builder()
                    .onHandshake((socket, handshake) -> {
                        socket.sendCloseFrame()
                    })
                    .build()
    
        when:
            ServerListener listener = listener(connectionListener)
            Server server = Server.server(CONFIGURATION, listener)
            server.startOnNewThread()
            client.connect(createListener(closeFrameHandler))
        then:
            closeFrameHandler.invoked().code == CloseFrame.CODE_NORMAL_CLOSE
        cleanup:
            server.close()
            client.close()
        
    }
    
    private static ServerListener listener(WebSocketConnectionListener connectionListener)
    {
        return new WebsocketProtocol(connectionListener)
    }
    
    private static WebSocketClientListener createListener(Handler<CloseFrame> closeFrameHandler)
    {
        return new WebSocketClientListener() {
            
            @Override
            void onCloseFrame(CloseFrame closeFrame)
            {
                closeFrameHandler.invoke(closeFrame)
            }
        }
    }
}
