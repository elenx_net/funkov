package net.elenx.funkov.integrationtests.osr

import net.elenx.funkov.common.properties.Properties
import net.elenx.funkov.server.reactor.Reactor
import net.elenx.funkov.server.worker.Worker
import spock.lang.Ignore
import spock.lang.Specification

import java.nio.charset.StandardCharsets
import java.nio.file.Path
import java.nio.file.Paths

class OSROptimizationTest extends Specification
{

    @Ignore // it works just ignored because of CI
    def "should perform C2 compilation on critical paths"()
    {
        given:
            Set<FunkovMethod> methodsWhichRequiresCompilation = prepareMethodNames()
            Properties.load(Paths.get("local", "app.config"))
            Path jarPath = buildExecutableJar()
            String warmupIterations = "15000"
            String closeServerAfterWarmup = "true"
            Path logsFile = Paths.get("target", "jit_logs.txt")
        when:
            Process runServerWithWarmupIterations = new ProcessBuilder(
                    Properties.get("jdk15.home", String.class) + Paths.get("bin", "java").toString(), "-jar", "--enable-preview",
                    "-XX:+UnlockDiagnosticVMOptions", "-XX:+LogCompilation", "-XX:LogFile=" + logsFile.toString(),
                    jarPath.toString(),
                    warmupIterations, closeServerAfterWarmup)
                    .start()

            String output = new String(runServerWithWarmupIterations.errorStream.readAllBytes(), StandardCharsets.ISO_8859_1)
            println(output)
            String output2 = new String(runServerWithWarmupIterations.in.readAllBytes(), StandardCharsets.ISO_8859_1)
            println(output2)
        then:
            methodsWhichRequiresCompilation.stream().allMatch { compiledByC4AndNotDeoptimized(logsFile, it.className + " " + it.methodName) }
    }

    private static Set<FunkovMethod> prepareMethodNames()
    {
        return [
                FunkovMethod.from(Worker.class, "runMainLoop"),
                FunkovMethod.from(Reactor.class, "handleSelectionKeys"),
        ].toSet()
    }

    private static Path buildExecutableJar()
    {
        Path projectRootDirectory = Paths.get(System.getProperty("user.dir")).parent
        boolean isWindows = System.getProperty("os.name").containsIgnoreCase("windows")
        ProcessBuilder pBuilder = new ProcessBuilder(isWindows ? "mvnw.cmd" : "./mvnw", "package", "-DskipTests").directory(projectRootDirectory.toFile())
        pBuilder.environment().put("JAVA_HOME", Properties.get("jdk15.home", String.class))
        Process p = pBuilder.start()

        String output = new String(p.inputStream.readAllBytes(), StandardCharsets.ISO_8859_1)
        println(output)
        return Paths.get(output.lines().filter { it.contains("[INFO] Building jar") }.filter{it.contains("integrationtests")}.findAny().get().split(" ")[3])
    }

    private static boolean compiledByC4AndNotDeoptimized(Path logFile, String method) {
        FileInputStream fstream = new FileInputStream(logFile.toString())
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream))
        String strLine
        boolean compiledByC4 = false
        boolean deoptimized = false

        String compileId = null
        while ((strLine = br.readLine()) != null)
        {
            if(!compiledByC4 && strLine.contains("compiler='c2' level='4'") && strLine.contains(method))
            {
                compiledByC4 = true
                compileId = strLine.find("compile_id='(.*?)'")
            }

            if(compiledByC4)
            {
                if(strLine.contains(compileId) && strLine.contains("deoptimized"))
                {
                    deoptimized = true
                }
            }
        }
        fstream.close()
        println("Method " + method + "\ncompiled = " + compiledByC4 + "\ndeoptimized = " + deoptimized)
        return compiledByC4 && !deoptimized
    }

    private static class FunkovMethod {

        private final String className
        private final String methodName

        FunkovMethod(String className, String methodName)
        {
            this.className = className
            this.methodName = methodName
        }

        String getClassName()
        {
            return className
        }

        String getMethodName()
        {
            return methodName
        }

        static <T> FunkovMethod from(Class<T> classType, String methodName)
        {
            return new FunkovMethod(classType.name, methodName)
        }
    }
}
