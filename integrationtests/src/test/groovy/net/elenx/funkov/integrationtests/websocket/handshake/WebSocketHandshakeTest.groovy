package net.elenx.funkov.integrationtests.websocket.handshake

import net.elenx.funkov.client.WebSocketClient
import net.elenx.funkov.client.WebSocketClientFactory
import net.elenx.funkov.client.WebSocketClientListener
import net.elenx.funkov.common.handler.Handler
import net.elenx.funkov.common.handler.Handlers
import net.elenx.funkov.integrationtests.server.ServerRunner
import net.elenx.funkov.model.Handshake
import net.elenx.funkov.model.MutableHandshake
import net.elenx.funkov.protocol.websocket.WebsocketProtocol
import net.elenx.funkov.server.Server
import net.elenx.funkov.server.configuration.FunkovServerConfiguration
import net.elenx.funkov.server.reactor.Reactor
import spock.lang.Shared
import spock.lang.Specification

class WebSocketHandshakeTest extends Specification
{
    @Shared
    int port = 5555
    @Shared
    URI SERVER_URI = new URI("ws://127.0.0.1:${port}")
    
    WebSocketClient client
    Reactor server
    
    Handler<Handshake> handshakeHandler
    
    void setup()
    {
        handshakeHandler = Handlers.handler(Handshake.class)
        client = WebSocketClientFactory.nettyClient(SERVER_URI.toString())
        FunkovServerConfiguration configuration = FunkovServerConfiguration.builder()
                .port(port)
                .workerThreads(1)
                .build()
        server = Server.server(configuration, new WebsocketProtocol(new ServerRunner.MockedWebSocketConnectionListener())) as Reactor
        
        server.startOnNewThread()
        client.connect(createListener(handshakeHandler))
    }
    
    void cleanup()
    {
        client.close()
        server.close()
    }
    
    void "Should perform webSocket handshake between client and server"()
    {
        expect:
            (handshakeHandler.invoked() as MutableHandshake).accepted
    }
    
    private static WebSocketClientListener createListener(Handler<Handshake> handshakeHandler)
    {
        return new WebSocketClientListener() {
            @Override
            void onConnect(Handshake handshake)
            {
                handshakeHandler.invoke(handshake)
            }
        }
    }
}