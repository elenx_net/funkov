package net.elenx.funkov.protocol.websocket.handshake

import net.elenx.funkov.model.Handshake
import net.openhft.chronicle.bytes.Bytes
import spock.lang.Specification
import spock.lang.Unroll

import java.nio.ByteBuffer
import java.util.stream.Collectors

class HandshakeDecoderTest extends Specification
{
    
    static final DEFAULT_HTTP_METHOD = "GET"
    static final DEFAULT_HTTP_PROTOCOL = "HTTP/1.1"
    
    Bytes<ByteBuffer> bytes
    
    void setup()
    {
        given:
            bytes = Bytes.elasticByteBuffer(64, 512)
    }
    
    void "Should accept handshake"()
    {
        given:
            Map<String, String> defaultHeaders = createDefaultHeaders()
            String req = createRequest(DEFAULT_HTTP_METHOD, DEFAULT_HTTP_PROTOCOL, defaultHeaders)
            bytes.write(req)
            bytes.underlyingObject().position((int) bytes.writePosition())
        
        when:
            Handshake handshake = new HandshakeDecoder().decode(bytes)
        then:
            handshake.accepted
            handshake.resourceUri() == "/"
            handshake.httpVersion() == DEFAULT_HTTP_PROTOCOL
            handshake.host() == defaultHeaders.get("host")
    }
    
    @Unroll()
    void "Should reject handshake when headers do not conform websocket specification"()
    {
        given:
            String req = createRequest(DEFAULT_HTTP_METHOD, DEFAULT_HTTP_PROTOCOL, createRequestHeaders(headerName, value, isPresent))
            bytes.write(req)
            bytes.underlyingObject().position((int) bytes.writePosition())
        
        when:
            Handshake handshake = new HandshakeDecoder().decode(bytes)
        
        then:
            !handshake.accepted
            handshake.httpVersion() == DEFAULT_HTTP_PROTOCOL
        
        where:
            headerName              | value      | isPresent
            "host"                  | null       | false
            "upgrade"               | "htp"      | true
            "upgrade"               | null       | false
            "connection"            | "Update"   | true
            "connection"            | null       | false
            "sec-websocket-key"     | "213yasdj" | true
            "sec-websocket-key"     | null       | false
            "sec-websocket-version" | "12"       | true
            "sec-websocket-version" | null       | false
    }
    
    @Unroll()
    void "Should reject handshake when protocol method/version do not conform websocket specification"()
    {
        given:
            String req = createRequest(method, protocol, createDefaultHeaders())
            bytes.write(req)
            bytes.underlyingObject().position((int) bytes.writePosition())
        
        when:
            Handshake handshake = new HandshakeDecoder().decode(bytes)
        
        then:
            !handshake.accepted
        where:
            method | protocol
            "GET"  | "HTTP/1.0"
            "PUT"  | "HTTP/1.1"
    }
    
    void "Reading header names should be case insensitive"()
    {
        given:
            Map<String, String> defaultHeaders = createDefaultHeaders()
            def headersIterator = defaultHeaders.iterator()
            Map<String, String> caseShuffledHeaderNames = new HashMap<>()
            
            while (headersIterator.hasNext())
            {
                def entry = headersIterator.next()
                headersIterator.remove()
                caseShuffledHeaderNames.put(shuffleCaseSensitive(entry.getKey()), entry.getValue())
            }
            
            defaultHeaders = caseShuffledHeaderNames
            
            String req = createRequest(DEFAULT_HTTP_METHOD, DEFAULT_HTTP_PROTOCOL, defaultHeaders)
            bytes.write(req)
            bytes.underlyingObject().position((int) bytes.writePosition())
        
        when:
            Handshake handshake = new HandshakeDecoder().decode(bytes)
        then:
            handshake.accepted
            handshake.resourceUri() == "/"
            handshake.httpVersion() == DEFAULT_HTTP_PROTOCOL
    }
    
    private static String createRequest(String httpMethod, String httpProtocol, Map<String, String> headers)
    {
        return headers.entrySet().stream()
                .map(entry -> "$entry.key: $entry.value")
                .collect(Collectors.joining("\r\n", "$httpMethod / $httpProtocol\r\n", "")) + "\r\n\r\n"
    }
    
    private static Map<String, String> createRequestHeaders(String headerName, String headerValue, boolean isPresent)
    {
        Map<String, String> defaultHeaders = createDefaultHeaders()
        if (isPresent)
        {
            defaultHeaders.put(headerName, headerValue)
        } else
        {
            defaultHeaders.remove(headerName)
        }
        return defaultHeaders
    }
    
    private static Map<String, String> createDefaultHeaders()
    {
        return new HashMap<>(Map.of(
                "host", "example.com",
                "upgrade", "websocket",
                "connection", "Upgrade",
                "sec-websocket-key", "dGhlIHNhbXBsZSBub25jZQ==",
                "sec-websocket-version", "13"
        ))
    }
    
    static String shuffleCaseSensitive(String headerName)
    {
        StringBuilder sb = new StringBuilder()
        for (int i = 0; i < headerName.length(); i++)
            sb.append( (i % 2) == 0 ? headerName.charAt(i).toUpperCase() : headerName.charAt(i))
        return sb.toString()
    }
}
