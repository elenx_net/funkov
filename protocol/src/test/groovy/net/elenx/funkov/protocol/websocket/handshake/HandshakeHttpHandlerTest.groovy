package net.elenx.funkov.protocol.websocket.handshake

import net.elenx.funkov.model.MutableHandshake
import net.openhft.chronicle.bytes.Bytes
import spock.lang.Specification

import java.nio.ByteBuffer
import java.nio.charset.StandardCharsets

class HandshakeHttpHandlerTest extends Specification
{
    
    Bytes<ByteBuffer> bytes
    
    void setup()
    {
        bytes = Bytes.elasticByteBuffer(512, 512)
    }
    
    void "Should process handshake on partial writes"()
    {
        
        given:
            HandshakeHandler customHandshakeHandler = new HandshakeHandler()
            byte[] part1 = "GET / HTTP/1.1\r\n".getBytes(StandardCharsets.UTF_8)
            byte[] part2 = "host: example.com\r\n".getBytes(StandardCharsets.UTF_8)
            byte[] part3 = "connection: Upgrade\r\n".getBytes(StandardCharsets.UTF_8)
            byte[] part4 = "upgrade: websocket\r\n".getBytes(StandardCharsets.UTF_8)
            byte[] part5 = "sec-websocket-key: dGhlIHNhbXBsZSBub25jZQ==\r\n".getBytes(StandardCharsets.UTF_8)
            byte[] part6 = "sec-websocket-version: 13\r\n\r\n".getBytes(StandardCharsets.UTF_8)
        
        expect:
            customHandshakeHandler.handle(bytes) == null
        
        when:
            bytes.write(part1)
        then:
            customHandshakeHandler.handle(bytes) == null
        
        when:
            bytes.write(part2)
            bytes.write(part3)
            bytes.write(part4)
            bytes.write(part5)
        then:
            customHandshakeHandler.handle(bytes) == null
        
        when:
            bytes.write(part6)
        then:
            (customHandshakeHandler.handle(bytes) as MutableHandshake).accepted
    }
    
    void "Should reject handshake on malformed request"()
    {
        given:
            byte[] req = "\r\n\r\n".getBytes(StandardCharsets.UTF_8)
        
        when:
            bytes.write(req)
        
        then:
            !(new HandshakeHandler().handle(bytes) as MutableHandshake).accepted
    }
}
