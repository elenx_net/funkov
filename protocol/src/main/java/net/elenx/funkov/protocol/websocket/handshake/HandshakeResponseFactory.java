package net.elenx.funkov.protocol.websocket.handshake;

import net.elenx.funkov.common.SneakyThrow;
import net.elenx.funkov.model.Handshake;
import net.elenx.funkov.model.MutableHandshake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import static net.elenx.funkov.protocol.websocket.handshake.HandshakeHandler.HTTP_1_1;
import static net.elenx.funkov.protocol.websocket.handshake.HandshakeHandler.HTTP_2_0;
import static net.elenx.funkov.protocol.websocket.handshake.HandshakeHandler.HTTP_3_0;

public class HandshakeResponseFactory
{
    private static final int WEBSOCKET_KEY_BYTES_LENGTH = 28;
    private static final byte[] RNRN = "\r\n\r\n".getBytes();

    private static final Logger log = LoggerFactory.getLogger(HandshakeResponseFactory.class);

    private static final String BAD_REQUEST_CODE = " 400 Bad Request";
    private static final byte[] BAD_REQUEST_HTTP_1_1 = (HTTP_1_1 + BAD_REQUEST_CODE).getBytes();
    private static final byte[] BAD_REQUEST_HTTP_2_0 = (HTTP_2_0 + BAD_REQUEST_CODE).getBytes();
    private static final byte[] BAD_REQUEST_HTTP_3_0 = (HTTP_3_0 + BAD_REQUEST_CODE).getBytes();

    private final byte[] SWITCH_PROTOCOLS_HTTP_1_1;
    private final byte[] SWITCH_PROTOCOLS_HTTP_2_0;
    private final byte[] SWITCH_PROTOCOLS_HTTP_3_0;

    public HandshakeResponseFactory()
    {
        String response = """
                101 Switching Protocols\r
                Upgrade: websocket\r
                Connection: Upgrade\r
                Sec-WebSocket-Accept: """;

        byte[] responseHttp_1_1 = (HTTP_1_1 + " " + response + " ").getBytes();
        byte[] responseHttp_2_0 = (HTTP_2_0 + " " + response + " ").getBytes();
        byte[] responseHttp_3_0 = (HTTP_3_0 + " " + response + " ").getBytes();

        SWITCH_PROTOCOLS_HTTP_1_1 = new byte[responseHttp_1_1.length + WEBSOCKET_KEY_BYTES_LENGTH + RNRN.length];
        System.arraycopy(responseHttp_1_1, 0, SWITCH_PROTOCOLS_HTTP_1_1, 0, responseHttp_1_1.length);
        System.arraycopy(RNRN, 0, SWITCH_PROTOCOLS_HTTP_1_1, SWITCH_PROTOCOLS_HTTP_1_1.length - 4, RNRN.length);

        SWITCH_PROTOCOLS_HTTP_2_0 = new byte[responseHttp_2_0.length + WEBSOCKET_KEY_BYTES_LENGTH + RNRN.length];
        System.arraycopy(responseHttp_2_0, 0, SWITCH_PROTOCOLS_HTTP_2_0, 0, responseHttp_2_0.length);
        System.arraycopy(RNRN, 0, SWITCH_PROTOCOLS_HTTP_2_0, SWITCH_PROTOCOLS_HTTP_2_0.length - 4, RNRN.length);

        SWITCH_PROTOCOLS_HTTP_3_0 = new byte[responseHttp_3_0.length + WEBSOCKET_KEY_BYTES_LENGTH + RNRN.length];
        System.arraycopy(responseHttp_3_0, 0, SWITCH_PROTOCOLS_HTTP_3_0, 0, responseHttp_3_0.length);
        System.arraycopy(RNRN, 0, SWITCH_PROTOCOLS_HTTP_3_0, SWITCH_PROTOCOLS_HTTP_3_0.length - 4, RNRN.length);
    }

    public byte[] createResponse(MutableHandshake handshake)
    {
        return handshake.isAccepted() ? successfulResponse(handshake) : failureResponse(handshake);
    }

    private byte[] successfulResponse(MutableHandshake handshake)
    {
        byte[] webSocketAcceptHeaderValue = generateWebSocketAccept(handshake.getWebsocketKey());
        byte[] response = arrayForHeader(handshake.httpVersion());
        System.arraycopy(webSocketAcceptHeaderValue, 0, response, 97, webSocketAcceptHeaderValue.length);

        return response;
    }

    private byte[] arrayForHeader(String protocol)
    {
        switch (protocol)
        {
            case HTTP_1_1:
                return SWITCH_PROTOCOLS_HTTP_1_1;
            case HTTP_2_0:
                return SWITCH_PROTOCOLS_HTTP_2_0;
            case HTTP_3_0:
                return SWITCH_PROTOCOLS_HTTP_3_0;
            default:
                return SWITCH_PROTOCOLS_HTTP_1_1;
        }
    }

    // key already contains contacted WEB_SOCKET_GUID
    private byte[] generateWebSocketAccept(byte[] webSocketKey)
    {
        try
        {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            digest.reset();
            byte[] bytes = digest.digest(webSocketKey);
            return Base64.getEncoder().encode(bytes);
        } catch (NoSuchAlgorithmException ex) //will never happen
        {
            log.error(ex.getMessage(), ex);
            SneakyThrow.sneakyThrow(ex);
            return new byte[]{};
        }
    }

    private byte[] failureResponse(Handshake handshake)
    {
        String httpVersion = handshake.httpVersion();

        if (httpVersion != null)
        {
            switch (httpVersion)
            {
                case HTTP_1_1:
                    return BAD_REQUEST_HTTP_1_1;
                case HTTP_2_0:
                    return BAD_REQUEST_HTTP_2_0;
                case HTTP_3_0:
                    return BAD_REQUEST_HTTP_3_0;
            }
        }
        return BAD_REQUEST_HTTP_1_1;
    }
}
