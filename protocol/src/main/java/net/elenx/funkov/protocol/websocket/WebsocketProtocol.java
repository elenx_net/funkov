package net.elenx.funkov.protocol.websocket;

import net.elenx.funkov.common.annotations.ThreadSafeContext;
import net.elenx.funkov.serverapi.ConnectionListener;
import net.elenx.funkov.serverapi.ServerListener;

import java.nio.channels.SocketChannel;
import java.util.Optional;

@ThreadSafeContext
public class WebsocketProtocol implements ServerListener
{
    private final WebSocketConnectionListener connectionListener;

    public WebsocketProtocol(WebSocketConnectionListener connectionListener)
    {
        this.connectionListener = connectionListener;
    }

    @Override
    public void onStartServer()
    {
        connectionListener.onStartServer();
    }

    @Override
    public Optional<ConnectionListener> onConnectionAccept(SocketChannel clientSocket)
    {
        return Optional.of(new WebsocketConnectionServerListener(connectionListener));
    }
}
