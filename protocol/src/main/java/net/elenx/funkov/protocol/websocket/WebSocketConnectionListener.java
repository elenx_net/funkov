package net.elenx.funkov.protocol.websocket;

import net.elenx.funkov.common.annotations.ThreadSafeContext;
import net.elenx.funkov.model.Frame;
import net.elenx.funkov.model.Handshake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;

@ThreadSafeContext
public interface WebSocketConnectionListener
{
    Logger LOG = LoggerFactory.getLogger(WebSocketConnectionListener.class);

    default void onMessage(WebSocket socket, String message) {}
    default void onMessage(WebSocket socket, ByteBuffer message) {}
    default void onContinuationFrame(WebSocket socket, Frame frame) {}
    default void onPingMessage( WebSocket socket, Frame frame ) {}
    default void onPongMessage( WebSocket socket, Frame frame ) {}
    default void onConnectionError(Exception ex) {}
    default void onProcessingMessageError(WebSocket socket, Exception ex) {}
    default void onStartServer() {}
    default void onHandshake(WebSocket socket, Handshake handshake) {
        LOG.info("Handshake status: {}", handshake);
    }
    default void onCloseConnectionInitiatedByClient(WebSocket socket, String reason, int code) {}
    default void onCloseConnectionInitiatedByServer(String reason, int code) {}

}
