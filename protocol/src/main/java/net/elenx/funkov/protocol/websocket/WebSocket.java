package net.elenx.funkov.protocol.websocket;

import net.elenx.funkov.model.CloseFrame;

import java.util.ArrayList;
import java.util.List;

public class WebSocket
{
    private final List<CloseFrame> framesToSend = new ArrayList<>();

    public void sendCloseFrame()
    {
        framesToSend.add(CloseFrame.CLOSE_FRAME);
    }

    public List<CloseFrame> getFramesToSend()
    {
        return framesToSend;
    }

    public int clearFrames()
    {
        int elementsCount = framesToSend.size();
        framesToSend.clear();
        return elementsCount;
    }
}
