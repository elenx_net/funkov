package net.elenx.funkov.protocol.websocket.handshake;

import net.elenx.funkov.model.MutableHandshake;
import net.openhft.chronicle.bytes.Bytes;

import java.nio.ByteBuffer;

public class HandshakeHandler
{
    static final String HTTP_1_1 = "HTTP/1.1";
    static final String HTTP_2_0 = "HTTP/2.0";
    static final String HTTP_3_0 = "HTTP/3.0";

    private final HandshakeDecoder handshakeDecoder = new HandshakeDecoder();
    private final HandshakeResponseFactory responseFactory = new HandshakeResponseFactory();

    public MutableHandshake handle(Bytes<ByteBuffer> bytes)
    {
        MutableHandshake handshake = handshakeDecoder.decode(bytes);

        // not enough bytes to process message
        if (handshake == null)
        {
            return null;
        }

        bytes.writePosition(0);
        byte[] response = responseFactory.createResponse(handshake);
        bytes.write(response);
        return handshake;
    }
}
