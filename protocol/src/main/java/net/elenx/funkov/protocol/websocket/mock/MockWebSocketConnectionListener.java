package net.elenx.funkov.protocol.websocket.mock;

import net.elenx.funkov.model.Handshake;
import net.elenx.funkov.protocol.websocket.WebSocket;
import net.elenx.funkov.protocol.websocket.WebSocketConnectionListener;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class MockWebSocketConnectionListener implements WebSocketConnectionListener
{
    private final Consumer<Exception> onConnectionError;
    private final Runnable onStartServer;
    private final BiConsumer<WebSocket, Handshake> onHandshake;

    private MockWebSocketConnectionListener(Consumer<Exception> onConnectionError, Runnable onStartServer, BiConsumer<WebSocket, Handshake> onHandshake)
    {
        this.onConnectionError = onConnectionError;
        this.onStartServer = onStartServer;
        this.onHandshake = onHandshake;
    }

    public static MockWebSocketConnectionListener.Builder builder()
    {
        return new MockWebSocketConnectionListener.Builder();
    }

    @Override
    public void onConnectionError(Exception ex)
    {
        if (onConnectionError != null)
        {
            onConnectionError.accept(ex);
        }
    }

    @Override
    public void onStartServer()
    {
        if (onStartServer != null)
        {
            onStartServer.run();
        }
    }

    @Override
    public void onHandshake(WebSocket socket, Handshake handshake)
    {
        if (onHandshake != null)
        {
            onHandshake.accept(socket, handshake);
        }
    }

    public static class Builder
    {
        private Consumer<Exception> onConnectionError;
        private Runnable onStartServer;
        private BiConsumer<WebSocket, Handshake> onHandshake;

        public Builder onConnectionError(Consumer<Exception> onConnectionError)
        {
            this.onConnectionError = onConnectionError;
            return this;
        }

        public Builder onStartServer(Runnable onStartServer)
        {
            this.onStartServer = onStartServer;
            return this;
        }

        public Builder onHandshake(BiConsumer<WebSocket, Handshake> onHandshake)
        {
            this.onHandshake = onHandshake;
            return this;
        }

        public MockWebSocketConnectionListener build()
        {
            return new MockWebSocketConnectionListener(onConnectionError, onStartServer, onHandshake);
        }
    }
}
