package net.elenx.funkov.protocol.websocket.frame;

import net.elenx.funkov.model.CloseFrame;
import net.openhft.chronicle.bytes.Bytes;
import net.openhft.chronicle.core.util.StringUtils;

import java.nio.ByteBuffer;
import java.util.List;

public class FrameProcessor
{
    private FrameProcessor()
    {
    }

    public static void processFrames(Bytes<ByteBuffer> connectionByteBuffer, List<CloseFrame> frames) {
        frames.forEach(frame -> encodeCloseFrame(connectionByteBuffer, frame));
    }

    private static Bytes<ByteBuffer> encodeCloseFrame(Bytes<ByteBuffer> connectionByteBuffer, CloseFrame closeFrame) {
        int payloadLength = 2; // close frame code should be 2 bytes (short)

        short code = closeFrame.getCode();
        String reason = closeFrame.getReason();
        payloadLength += reason.length(); // not the case in UTF-16 encoding which we doesn't support now

        Bytes<ByteBuffer> byteBufferBytes = encodeFrame(connectionByteBuffer, closeFrame, payloadLength);
        byteBufferBytes.writeShort(Short.reverseBytes(code));
        byteBufferBytes.appendUtf8(StringUtils.extractBytes(reason), 0, reason.length(), StringUtils.getStringCoder(reason));

        return byteBufferBytes;
    }

    private static Bytes<ByteBuffer> encodeFrame(Bytes<ByteBuffer> payload, CloseFrame frame, long payloadLength) {
        byte[] payloadLengthBytes = calculatePayloadLength((int) payloadLength);
        int length = payloadLengthBytes.length;

        byte firstByte = -128;
        firstByte |= (byte) (frame.isRsv1() ? 64 : 0);
        firstByte |= (byte) (frame.isRsv2() ? 32 : 0);
        firstByte |= (byte) (frame.isRsv3() ? 16 : 0);
        firstByte |= frame.getOpCode();

        payload.writeByte(firstByte);
        if (length == 1)
            payload.writeByte(payloadLengthBytes[0]);
        else if (length == 2)
            payload.writeUnsignedByte(126).write(payloadLengthBytes);
        else
            payload.writeUnsignedByte(127).write(payloadLengthBytes);

        return payload;
    }

    public static byte[] calculatePayloadLength(int payloadLength) {
        byte requiredBytesAmount = checkRequiredBytesAmount(payloadLength);
        byte[] bytes = new byte[requiredBytesAmount];
        int shiftSize = 8 * requiredBytesAmount - 8;
        for (int i = 0; i < requiredBytesAmount; i++) {
            bytes[i] = (byte) (payloadLength >>> (shiftSize - 8 * i));
        }
        return bytes;
    }

    private static byte checkRequiredBytesAmount(int payloadSize) {
        if (payloadSize <= 125)
            return 1;
        else if (payloadSize <= 65535)
            return 2;
        else
            return 8;
    }
}
