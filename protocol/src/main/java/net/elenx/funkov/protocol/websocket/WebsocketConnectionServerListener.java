package net.elenx.funkov.protocol.websocket;

import io.netty.util.concurrent.FastThreadLocal;
import net.elenx.funkov.model.MutableHandshake;
import net.elenx.funkov.protocol.websocket.frame.FrameProcessor;
import net.elenx.funkov.protocol.websocket.handshake.HandshakeHandler;
import net.elenx.funkov.serverapi.Connection;
import net.elenx.funkov.serverapi.ConnectionListener;

class WebsocketConnectionServerListener implements ConnectionListener
{
    private static final FastThreadLocal<HandshakeHandler> handshakeHandler = new FastThreadLocal<>() {
        @Override
        protected HandshakeHandler initialValue()
        {
            return new HandshakeHandler();
        }
    };

    private final WebSocketConnectionListener webSocketConnectionListener;
    private boolean isHandshaked;

    private static final FastThreadLocal<WebSocket> webSocketThreadLocal = new FastThreadLocal<>() {
        @Override
        protected WebSocket initialValue()
        {
            return new WebSocket();
        }
    };

    public WebsocketConnectionServerListener(WebSocketConnectionListener webSocketConnectionListener)
    {

        this.webSocketConnectionListener = webSocketConnectionListener;
    }

    @Override
    public void onConnectionError(Exception ex, Connection connection)
    {
        webSocketConnectionListener.onConnectionError(ex);
    }

    @Override
    public boolean afterRead(Connection connection)
    {
        if (!isHandshaked)
        {
            MutableHandshake handshake = handshakeHandler.get().handle(connection.associatedByteBuffer());

            if (handshake == null)
                return false;

            boolean accepted = handshake.isAccepted();
            if (accepted)
            {
                isHandshaked = true;
                WebSocket webSocket = WebsocketConnectionServerListener.webSocketThreadLocal.get();
                webSocketConnectionListener.onHandshake(webSocket, handshake);
                FrameProcessor.processFrames(connection.associatedByteBuffer(), webSocket.getFramesToSend());
                webSocket.clearFrames();
            }

            return accepted;
        }

        throw new IllegalArgumentException("Not implemented yet");
    }

    @Override
    public void beforeServerClose()
    {
        handshakeHandler.remove();
        webSocketThreadLocal.remove();
    }
}
