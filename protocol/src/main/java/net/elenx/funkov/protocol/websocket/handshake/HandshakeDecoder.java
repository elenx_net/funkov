package net.elenx.funkov.protocol.websocket.handshake;

import net.elenx.funkov.common.ArrayCharSequence;
import net.elenx.funkov.common.OS;
import net.elenx.funkov.model.MutableHandshake;
import net.openhft.chronicle.bytes.Bytes;
import net.openhft.chronicle.core.pool.StringInterner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.Base64;

import static net.elenx.funkov.protocol.websocket.handshake.HandshakeHandler.HTTP_1_1;
import static net.elenx.funkov.protocol.websocket.handshake.HandshakeHandler.HTTP_2_0;
import static net.elenx.funkov.protocol.websocket.handshake.HandshakeHandler.HTTP_3_0;

public class HandshakeDecoder
{
    private static final Logger log = LoggerFactory.getLogger(HandshakeDecoder.class);

    private static final int MINIMUM_LENGTH_OF_CORRECT_REQUEST = 150;
    private static final int END_OF_HTTP_REQUEST_AS_INTEGER = 168626701; // /r/n/r/n
    private static final short RN = 2573; // /r/n

    private static final char[] HOST_HEADER = "host: ".toCharArray();
    private static final char[] UPGRADE_HEADER = "upgrade: ".toCharArray();
    private static final char[] UPGRADE_HEADER_VALUE = "websocket".toCharArray();
    private static final char[] CONNECTION_HEADER = "connection: ".toCharArray();
    private static final char[] CONNECTION_HEADER_VALUE = "upgrade".toCharArray();
    private static final char[] SEC_WEBSOCKET_HEADER = "sec-websocket-".toCharArray();
    private static final char[] SEC_WEBSOCKET_VERSION = "version: 13".toCharArray();
    private static final char[] SEC_WEBSOCKET_KEY = "key: ".toCharArray();

    private final byte[] clientWebSocketKey = new byte[24];
    private final MutableHandshake handshake = new MutableHandshake();
    private final ArrayCharSequence arrayCharSequence = new ArrayCharSequence(128);
    private final StringInterner stringPool = new StringInterner(128);

    public MutableHandshake decode(Bytes<ByteBuffer> bytes)
    {
        MutableHandshake localHandshake = this.handshake;
        try
        {
            if (!containsFullHandshakeRequest(bytes))
                return null;

            localHandshake.resetToNulls();
            readResourceUriAndHttpVersion(bytes, localHandshake);
            readHeaders(bytes, localHandshake);
            return localHandshake.accept();
        } catch (IllegalArgumentException exc)
        {
            return wrongInputHandshake(localHandshake);
        } catch (Exception e)
        {
            log.error("", e);
            return wrongInputHandshake(localHandshake);
        }
    }

    private MutableHandshake wrongInputHandshake(MutableHandshake localHandshake)
    {
        if (localHandshake.httpVersion() == null)
            localHandshake.setHttpVersion(HTTP_1_1);
        return localHandshake.revoke();
    }

    public boolean containsFullHandshakeRequest(Bytes<ByteBuffer> bytes)
    {
        long position = bytes.writePosition();
        if (position < 4)
            return false;

        boolean fullRequest = OS.memory().readInt(bytes.addressForRead(position - 4)) == END_OF_HTTP_REQUEST_AS_INTEGER;

        if (fullRequest && position < MINIMUM_LENGTH_OF_CORRECT_REQUEST)
            throw new IllegalArgumentException("Invalid HTTP request, doesn't meet the minimum length for proper HTTP request");

        return fullRequest;
    }

    private void readHeaders(Bytes<ByteBuffer> bytes, MutableHandshake localHandshake)
    {
        boolean foundHostHeader = false;
        boolean foundUpgradeHeader = false;
        boolean foundConnectionHeader = false;
        boolean foundSecWebSocketKey = false;
        boolean foundSecWebSocketVersion = false;

        loop:
        while (true)
        {
            char firstHeaderChar = bytes.readChar();

            if (firstHeaderChar == '\u0000')
                break;

            switch (firstHeaderChar)
            {
                case 'h':
                case 'H':
                    foundHostHeader = maybeHostHeader(bytes, localHandshake);
                    break;
                case 'u':
                case 'U':
                    foundUpgradeHeader = maybeUpgradeHeader(bytes);
                    break;
                case 'c':
                case 'C':
                    foundConnectionHeader = maybeConnectionHeader(bytes);
                    break;
                case 's':
                case 'S':
                {
                    boolean foundAnyOfSecHeaders = maybeAnyOfSecWebsocketHeaders(bytes);
                    if (foundAnyOfSecHeaders)
                    {
                        switch (bytes.readChar())
                        {
                            case 'k':
                            case 'K':
                                foundSecWebSocketKey = maybeWebSocketKeyHeader(bytes, localHandshake);
                                break;
                            case 'v':
                            case 'V':
                                foundSecWebSocketVersion = maybeWebSocketVersionHeader(bytes);
                                break;
                            default:
                                customHeader(bytes);
                        }
                    }
                    break;
                }
                case '\r':
                {
                    throwIfNotEqual(bytes.readChar(), '\n');
                    break loop;
                }
                default:
                    customHeader(bytes);
            }
        }

        if (!(foundHostHeader && foundUpgradeHeader && foundConnectionHeader && foundSecWebSocketKey && foundSecWebSocketVersion))
            throw new IllegalArgumentException();

    }

    private void customHeader(Bytes<ByteBuffer> bytes)
    {
        while (bytes.readChar() != '\r') ;
        throwIfNotEqual('\n', bytes.readChar());
    }

    private boolean maybeWebSocketVersionHeader(Bytes<ByteBuffer> bytes)
    {
        for (int i = 1; i < SEC_WEBSOCKET_VERSION.length; i++)
        {
            if (caseInsensitiveNonEqual(SEC_WEBSOCKET_VERSION[i], bytes.readChar()))
                return false;
        }

        if ((bytes.readShort() ^ RN) != 0)
            throw new IllegalArgumentException();

        return true;
    }

    private boolean maybeWebSocketKeyHeader(Bytes<ByteBuffer> bytes, MutableHandshake mutableHandshake)
    {
        for (int i = 1; i < SEC_WEBSOCKET_KEY.length; i++)
        {
            if (caseInsensitiveNonEqual(SEC_WEBSOCKET_KEY[i], bytes.readChar()))
                return false;
        }

        bytes.read(clientWebSocketKey);
        throwIfNotEqual('\r', bytes.readChar());
        throwIfNotEqual('\n', bytes.readChar());

        boolean isValidKey = Base64.getDecoder().decode(clientWebSocketKey).length == 16;

        if (isValidKey)
            System.arraycopy(clientWebSocketKey, 0, mutableHandshake.getWebsocketKey(), 0, clientWebSocketKey.length);

        return isValidKey;

    }

    private boolean maybeAnyOfSecWebsocketHeaders(Bytes<ByteBuffer> bytes)
    {
        for (int i = 1; i < SEC_WEBSOCKET_HEADER.length; i++)
        {
            if (caseInsensitiveNonEqual(SEC_WEBSOCKET_HEADER[i], bytes.readChar()))
                return false;
        }
        return true;
    }

    private boolean maybeConnectionHeader(Bytes<ByteBuffer> bytes)
    {
        return checkIfContainsHeaderValues(bytes, CONNECTION_HEADER, CONNECTION_HEADER_VALUE);
    }

    private boolean maybeUpgradeHeader(Bytes<ByteBuffer> bytes)
    {
        return checkIfContainsHeaderValues(bytes, UPGRADE_HEADER, UPGRADE_HEADER_VALUE);
    }

    private boolean checkIfContainsHeaderValues(Bytes<ByteBuffer> bytes, char[] headerName, char[] headerValue)
    {
        for (int i = 1; i < headerName.length; i++)
        {
            if (caseInsensitiveNonEqual(headerName[i], bytes.readChar()))
                return false;
        }

        for (char c : headerValue)
        {
            char readChar = bytes.readChar();
            if (caseInsensitiveNonEqual(c, readChar))
                return false;
        }

        if (bytes.readChar() != '\r')
            return false;

        throwIfNotEqual('\n', bytes.readChar());
        return true;
    }

    private boolean maybeHostHeader(Bytes<ByteBuffer> bytes, MutableHandshake localHandshake)
    {
        for (int i = 1; i < HOST_HEADER.length; i++)
        {
            if (caseInsensitiveNonEqual(HOST_HEADER[i], bytes.readChar()))
                return false;
        }

        char c;
        arrayCharSequence.reset();
        while ((c = bytes.readChar()) != '\r')
            arrayCharSequence.append(c);

        throwIfNotEqual('\n', bytes.readChar());

        String host = stringPool.intern(arrayCharSequence);
        localHandshake.setHost(host);
        return true;
    }

    public void readResourceUriAndHttpVersion(Bytes<ByteBuffer> bytes, MutableHandshake localHandshake)
    {
        // if not 'G','E','T',' '
        throwIfNotEqual(bytes.readInt(), 542393671);

        arrayCharSequence.reset();

        char c;
        while ((c = bytes.readChar()) != ' ')
            arrayCharSequence.append(c);

        if (arrayCharSequence.length() <= 1)
            throw new IllegalArgumentException();

        String resourceUri = stringPool.intern(arrayCharSequence);
        localHandshake.setResourceUri(resourceUri);

        // if not 'H''T'T''P'
        throwIfNotEqual(bytes.readInt(), 1347703880);

        int httpVersion = bytes.readInt();

        if ((httpVersion ^ 825110831) == 0)
        {
            localHandshake.setHttpVersion(HTTP_1_1);

        } else if ((httpVersion ^ 808333871) == 0)
        {
            localHandshake.setHttpVersion(HTTP_2_0);
        } else if ((httpVersion ^ 808334127) == 0)
        {
            localHandshake.setHttpVersion(HTTP_3_0);
        } else
        {
            throw new IllegalArgumentException();
        }

        if (bytes.readShort() != RN)
            throw new IllegalArgumentException();

    }

    private void throwIfNotEqual(char first, char second)
    {
        if ((first ^ second) != 0)
            throw new IllegalArgumentException();
    }

    private void throwIfNotEqual(int first, int second)
    {
        if ((first ^ second) != 0)
            throw new IllegalArgumentException();
    }

    private boolean caseInsensitiveNonEqual(char firstLowerCase, char second)
    {
        return firstLowerCase != second && firstLowerCase - 32 != second;
    }

}
