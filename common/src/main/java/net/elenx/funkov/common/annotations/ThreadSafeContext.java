package net.elenx.funkov.common.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Elements marked with this annotation are NOT thread safe by itself but are used in thread safe context. For example
 * if we can guarantee that at most one thread access data in the same time, or it's inside {@link ThreadLocal}.
 * <p>
 * Programmer should be very careful where it comes to refactor or enhancement elements annotated by {@link ThreadSafeContext}
 * <p>
 * The annotation doesn't have any special effects at runtime, it's discarded by compiler.
 */
@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.TYPE})
public @interface ThreadSafeContext
{
    String description() default "";
}
