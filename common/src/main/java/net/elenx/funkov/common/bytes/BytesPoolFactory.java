package net.elenx.funkov.common.bytes;

public class BytesPoolFactory
{
    private BytesPoolFactory()
    {
    }

    public static BytesPool referenceBytesPool(int maxPoolSize, int bufferSize)
    {
        return new ReferenceBytesPool(maxPoolSize, bufferSize);
    }
}
