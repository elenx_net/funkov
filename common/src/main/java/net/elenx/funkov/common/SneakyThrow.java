package net.elenx.funkov.common;

public class SneakyThrow
{
    public static <E extends Throwable> void sneakyThrow(Throwable e) throws E
    {
        throw (E) e;
    }
}
