package net.elenx.funkov.common;

public class Jvm
{
    public static long freeHeapMemory()
    {
        return Runtime.getRuntime().freeMemory();
    }

    public static void safePoint()
    {
        Safepoint.force();
    }

    public static void gc()
    {
        System.gc();
    }


    private static class Safepoint
    {
        // must be volatile
        private static volatile int one = 1;

        public static void force()
        {
            // trick only works from Java 9+
            for (int i = 0; i < one; i++) ;
        }
    }
}
