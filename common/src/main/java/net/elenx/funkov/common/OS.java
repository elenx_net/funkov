package net.elenx.funkov.common;

import java.net.ServerSocket;
import java.net.Socket;

import static net.elenx.funkov.common.SneakyThrow.sneakyThrow;

public class OS
{

    private OS()
    {
    }

    private static int tcpBufferSize;
    private static int availableProcessors;
    private static Memory memory;

    public static Memory memory()
    {
        if (memory == null)
            memory = new Memory();

        return memory;
    }

    public static int availableLogicalProcessors()
    {
        if (availableProcessors == 0)
            availableProcessors = Runtime.getRuntime().availableProcessors();

        return availableProcessors;
    }

    public static int tcpBufferSize()
    {
        if (tcpBufferSize == 0)
            tcpBufferSize = findOutBufferSize();

        return tcpBufferSize;
    }

    private static int findOutBufferSize()
    {
        try (ServerSocket ss = new ServerSocket(0))
        {
            try (Socket s = new Socket("localhost", ss.getLocalPort()))
            {
                s.setReceiveBufferSize(4 << 20);
                s.setSendBufferSize(4 << 20);
                return Math.min(s.getReceiveBufferSize(), s.getSendBufferSize());
            }
        } catch (Exception e)
        {
            sneakyThrow(e);
            return 0;
        }
    }
}
