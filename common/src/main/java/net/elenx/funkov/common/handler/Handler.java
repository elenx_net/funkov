package net.elenx.funkov.common.handler;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static net.elenx.funkov.common.SneakyThrow.sneakyThrow;

public class Handler<T>
{
    private final long timeout;
    private final TimeUnit timeUnit;
    private final Semaphore semaphore = new Semaphore(0);
    private final AtomicReference<T> ref = new AtomicReference<>(null);

    Handler(long timeout, TimeUnit timeUnit)
    {
        this.timeout = timeout;
        this.timeUnit = timeUnit;
    }

    public T invoked()
    {
        try
        {
            if (semaphore.tryAcquire(timeout, timeUnit))
            {
                return ref.getAndSet(null);
            }
        } catch (InterruptedException e)
        {
            sneakyThrow(e);
        }
        throw new IllegalStateException();
    }

    public void invoke(T obj)
    {
        ref.set(obj);
        semaphore.release();
    }
}
