package net.elenx.funkov.common;

import org.jetbrains.annotations.NotNull;

public class ArrayCharSequence implements Appendable, CharSequence
{
    private final char[] array;
    private int count;

    public ArrayCharSequence(int size)
    {
        this.array = new char[size];
    }

    @Override
    public Appendable append(char c)
    {
        array[count++] = c;
        return this;
    }

    @Override
    public Appendable append(CharSequence csq)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public Appendable append(CharSequence csq, int start, int end)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public int length()
    {
        return count + 1;
    }

    @Override
    public char charAt(int index)
    {
        return array[index];
    }

    @Override
    public CharSequence subSequence(int start, int end)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    @NotNull
    public String toString()
    {
        return new String(array, 0, count);
    }

    public void reset()
    {
        count = 0;
    }
}
