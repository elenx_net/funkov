package net.elenx.funkov.common.bytes;

import net.elenx.funkov.common.annotations.SynchronizationRequired;
import net.openhft.chronicle.bytes.Bytes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;

@SynchronizationRequired
class ReferenceBytesPool implements BytesPool
{
    private static final Logger log = LoggerFactory.getLogger(ReferenceBytesPool.class);

    private final Bytes<ByteBuffer>[] buffersPool;
    private final int maxPoolSize;
    private final int bufferSize;

    private int poolIndex;

    public ReferenceBytesPool(int maxPoolSize, int bufferSize)
    {
        log.info("Initializing bytes pool, maxPoolSize = {}, bufferSize = {} B", maxPoolSize, bufferSize);
        this.maxPoolSize = maxPoolSize;
        this.bufferSize = bufferSize;
        this.buffersPool = new Bytes[maxPoolSize];
    }

    @Override
    public Bytes<ByteBuffer> leaseBuffer()
    {
        if (poolIndex == 0)
            return Bytes.elasticByteBuffer(bufferSize, bufferSize);

        Bytes<ByteBuffer> buffer = buffersPool[--poolIndex];
        buffer.clear();
        buffer.underlyingObject().clear();
        return buffer;
    }

    @Override
    public void releaseBuffer(Bytes<ByteBuffer> buffer)
    {
        if (buffer.capacity() != bufferSize)
            throw new IllegalStateException("Buffer size " + buffer.capacity() + " different from declared " + bufferSize);

        if (poolIndex == maxPoolSize)
        {
            log.debug("BytesPoll is full (size = {}), releasing memory associated to buffer", maxPoolSize);
            buffer.releaseLast();
        } else
            buffersPool[poolIndex++] = buffer;
    }

    @Override
    public int size()
    {
        return poolIndex;
    }

    @Override
    public void clean()
    {
        log.info("Cleaning bytes pool");
        while (poolIndex > 0)
        {
            buffersPool[--poolIndex].releaseLast();
        }
    }
}
