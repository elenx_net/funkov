package net.elenx.funkov.common.handler;

import java.util.concurrent.TimeUnit;

public class Handlers
{
    private static final long DEFAULT_TIMEOUT = 5;
    private static final TimeUnit DEFAULT_TIME_UNIT = TimeUnit.SECONDS;

    public static BooleanHandler handler()
    {
        return new BooleanHandler(DEFAULT_TIMEOUT, DEFAULT_TIME_UNIT);
    }

    public static <T> Handler<T> handler(Class<T> type)
    {
        return new Handler<>(DEFAULT_TIMEOUT, DEFAULT_TIME_UNIT);
    }

    private Handlers()
    {
    }
}
