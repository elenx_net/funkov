package net.elenx.funkov.common.bytes;

import net.elenx.funkov.common.annotations.SynchronizationRequired;
import net.openhft.chronicle.bytes.Bytes;

import java.nio.ByteBuffer;

@SynchronizationRequired
public interface BytesPool
{

    /**
     * @return ByteBuffer backed by native memory, whose read/write position = 0, limit = capacity
     * Buffers' capacity is constant and preset in {@link BytesPoolFactory}
     * @throws OutOfMemoryError if pool is empty and there is no enough native memory to allocate the buffer.
     *                          Keep in mind that if -XX:MaxDirectMemorySize=N is not explicitly passed, then its value is equal to -Xmx
     */
    Bytes<ByteBuffer> leaseBuffer();


    /**
     * Puts ByteBuffer back to pool (or releases memory associated with this buffer if there is no enough space in pool)
     * Buffer doesn't have to be that one acquired by {@link #leaseBuffer()} but its capacity has to be in accordance
     * with declared in {@link BytesPoolFactory} to keep pool consistency.
     * If you need to just release memory without pooling, use {@link Bytes#releaseLast()}.
     *
     * Buffer should not be used after releasing.
     *
     * @param buffer ByteBuffer backed by native memory.
     * @throws IllegalStateException if buffer capacity is different from preset in {@link BytesPoolFactory}
     */
    void releaseBuffer(Bytes<ByteBuffer> buffer);

    /**
     * @return current amount of buffers ready to use without additional allocation
     */
    int size();


    /**
     * Cleans bytes pool, releasing all reserved memory - its adequate to calling free() in C
     */
    void clean();
}
