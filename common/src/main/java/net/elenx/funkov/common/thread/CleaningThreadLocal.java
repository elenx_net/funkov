package net.elenx.funkov.common.thread;

import io.netty.util.concurrent.FastThreadLocal;
import net.openhft.chronicle.core.Jvm;
import net.openhft.chronicle.core.util.ThrowingConsumer;

import java.util.function.Supplier;
import java.util.function.UnaryOperator;

public class CleaningThreadLocal<T> extends ThreadLocal<T>
{
    private final UnaryOperator<T> getWrapper;
    private final ThrowingConsumer<T, Exception> cleanup;
    private final FastThreadLocal<T> fastThreadLocal;

    CleaningThreadLocal(Supplier<T> supplier, ThrowingConsumer<T, Exception> cleanup)
    {
        this(supplier, cleanup, UnaryOperator.identity());
    }

    CleaningThreadLocal(Supplier<T> supplier, ThrowingConsumer<T, Exception> cleanup, UnaryOperator<T> getWrapper)
    {
        this.cleanup = cleanup;
        this.getWrapper = getWrapper;
        this.fastThreadLocal = new FastThreadLocal<>()
        {

            @Override
            protected T initialValue()
            {
                return supplier.get();
            }
        };
    }

    public static <T> CleaningThreadLocal<T> withCleanup(Supplier<T> supplier, ThrowingConsumer<T, Exception> cleanup)
    {
        return new CleaningThreadLocal<>(supplier, cleanup);
    }

    @Override
    protected T initialValue()
    {
        return fastThreadLocal.get();
    }

    @Override
    public T get()
    {
        return getWrapper.apply(fastThreadLocal.get());
    }

    @Override
    public void remove()
    {
        fastThreadLocal.remove();
        super.remove();
    }

    public synchronized void cleanup(T value)
    {
        try
        {
            ThrowingConsumer<T, Exception> cleanup = this.cleanup;
            if (cleanup != null)
                cleanup.accept(value);
        } catch (Exception e)
        {
            Jvm.warn().on(getClass(), "Exception cleaning up " + value.getClass(), e);
        }
    }
}

