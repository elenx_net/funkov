package net.elenx.funkov.common.handler;

import java.util.concurrent.TimeUnit;

public class BooleanHandler
{
    private final Handler<Boolean> handler;

    BooleanHandler(long timeout, TimeUnit timeUnit)
    {
        this.handler = new Handler<>(timeout, timeUnit);
    }

    public boolean invoked()
    {
        return handler.invoked();
    }

    public void invoke()
    {
        handler.invoke(true);
    }
}
