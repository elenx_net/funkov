package net.elenx.funkov.common.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Elements marked with this annotation aren't thread safe, what means programmer should take care on his own of any
 * concurrency issues if its going to use in multithreaded environment.
 * <p>
 * The annotation doesn't have any special effects at runtime, it's discarded by compiler.
 */
@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.TYPE})
public @interface SynchronizationRequired
{
    String description() default "";
}
