package net.elenx.funkov.common.bytes;

import net.openhft.chronicle.bytes.Bytes;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class BytesToSocket
{
    private BytesToSocket()
    {
    }

    public static int read(Bytes<ByteBuffer> bytes, SocketChannel socketChannel) throws IOException
    {
        int readBytes = socketChannel.read(bytes.underlyingObject());

        if(readBytes != -1)
            bytes.writePosition(bytes.writePosition() + readBytes);

        return readBytes;
    }

    public static int write(Bytes<ByteBuffer> bytes, SocketChannel socketChannel) throws IOException
    {
        flip(bytes);
        return socketChannel.write(bytes.underlyingObject());

    }

    private static void flip(Bytes<ByteBuffer> bytes)
    {
        bytes.underlyingObject().position((int) bytes.readPosition());
        bytes.underlyingObject().limit((int) bytes.writePosition());
    }
}
