package net.elenx.funkov.common;

import org.jetbrains.annotations.NotNull;
import sun.misc.Unsafe;

import java.lang.invoke.VarHandle;
import java.lang.reflect.Field;

public class Memory
{
    Memory()
    {
    }

    private static int pageSize;
    private static final Unsafe UNSAFE;

    static
    {
        try
        {
            Field theUnsafe = Unsafe.class.getDeclaredField("theUnsafe");
            theUnsafe.setAccessible(true);
            UNSAFE = (Unsafe) theUnsafe.get(null);
        } catch (@NotNull NoSuchFieldException | IllegalAccessException | IllegalArgumentException e)
        {
            throw new AssertionError(e);
        }
    }

    public long allocate(long size)
    {
        return UNSAFE.allocateMemory(size);
    }

    public void putBooleanVolatile(long address, boolean value)
    {
        UNSAFE.putBooleanVolatile(null, address, value);
    }

    public boolean getBooleanVolatile(long address)
    {
        return UNSAFE.getBooleanVolatile(null, address);
    }

    public void putByteVolatile(long address, byte value)
    {
        UNSAFE.putByteVolatile(null, address, value);
    }

    public byte getByteVolatile(long address)
    {
        return UNSAFE.getByteVolatile(null, address);
    }

    public int readInt(long address)
    {
        return UNSAFE.getInt(address);
    }

    public byte readByte(long address)
    {
        return UNSAFE.getByte(address);
    }

    public int pageSize()
    {
        if (pageSize == 0)
            pageSize = UNSAFE.pageSize();

        return pageSize;
    }

    public static long arrayBaseOffset(Class<?> type)
    {
        return UNSAFE.arrayBaseOffset(type);
    }

    public static void putLong(Object[] array, long baseOffset, long address)
    {
        UNSAFE.putLong(array, baseOffset, address);
    }

    public static void acquireFence()
    {
        VarHandle.acquireFence();
    }

    public static void releaseFence()
    {
        VarHandle.releaseFence();
    }

    public static void loadLoadFence()
    {
        VarHandle.loadLoadFence();
    }

    public static void storeStoreFence()
    {
        VarHandle.storeStoreFence();
    }

    public static void fullFence()
    {
        VarHandle.fullFence();
    }
}
