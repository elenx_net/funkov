package net.elenx.funkov.common.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Elements marked with this annotation are thread safe. Can be used in multithreaded environment without any
 * concurrency issues.
 *
 * <p>
 * The annotation doesn't have any special effects at runtime, it's discarded by compiler.
 */
@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.TYPE})
public @interface ThreadSafe
{
    String description() default "";
}
