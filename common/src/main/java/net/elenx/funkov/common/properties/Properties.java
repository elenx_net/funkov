package net.elenx.funkov.common.properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.nio.file.Path;

public class Properties
{
    private static final Logger log = LoggerFactory.getLogger(Properties.class);

    private static java.util.Properties properties = new java.util.Properties();

    public static <T> T get(String key, Class<T> type)
    {
        String property = properties == null ? null : properties.getProperty(key);
        if(property == null)
        {
            property = System.getProperties().getProperty(key);
            if(property == null)
            {
                throw new IllegalStateException("Missing " + key + " property in configuration file");
            }
        }
        return (T) getValue(property, type);
    }

    @SuppressWarnings("unchecked")
    public static <T> T get(String key, T defaultValue, Class<T> type)
    {
        String property = properties == null ? null : properties.getProperty(key);
        if(property == null)
        {
            property = System.getProperties().getProperty(key);
            if(property == null)
            {
                return defaultValue;
            }
        }
        return (T) getValue(property, type);
    }

    private static Object getValue(String value, Class<?> type) {
        if (type == String.class)
            return value;
        if (type == boolean.class || type == Boolean.class)
            return Boolean.parseBoolean(value);
        if (type == int.class || type == Integer.class)
            return Integer.parseInt(value);
        if (type == float.class || type == Float.class)
            return Float.parseFloat(value);
        throw new IllegalArgumentException("Unknown configuration value type: " + type.getName());
    }

    public static void load(Path path)
    {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();

        var prop = new java.util.Properties();

        try(InputStream is = loader.getResourceAsStream(path.toString()))
        {
            prop.load(is);
        } catch (Exception e)
        {
            log.error("", e);
        }
        properties.putAll(prop);
    }

    public static void put(String key, Object value)
    {
        properties.put(key, value);
    }
}
