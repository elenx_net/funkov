package net.elenx.funkov.common.bytes

import net.openhft.chronicle.bytes.Bytes
import spock.lang.Specification

import java.nio.ByteBuffer

class BytesPoolTest extends Specification
{
    def "should return correct value of pool size"()
    {
        given:
            BytesPool bytesPool = BytesPoolFactory.referenceBytesPool(5, 5)
        when:
            int poolSize = bytesPool.size()
        then:
            poolSize == 0
        when:
            Bytes<ByteBuffer> buf1 = bytesPool.leaseBuffer()
            Bytes<ByteBuffer> buf2 = bytesPool.leaseBuffer()
            bytesPool.releaseBuffer(buf1)
            bytesPool.releaseBuffer(buf2)
        then:
            bytesPool.size() == 2
        when:
            bytesPool.leaseBuffer()
        then:
            bytesPool.size() == 1
    }
    
    def "should drop released buffers if there is no enough space in pool"()
    {
        given:
            BytesPool bytesPool = BytesPoolFactory.referenceBytesPool(2, 5)
            Bytes<ByteBuffer> buf1 = bytesPool.leaseBuffer()
            Bytes<ByteBuffer> buf2 = bytesPool.leaseBuffer()
            Bytes<ByteBuffer> buf3 = bytesPool.leaseBuffer()
        when:
            bytesPool.releaseBuffer(buf1)
            bytesPool.releaseBuffer(buf2)
            bytesPool.releaseBuffer(buf3)
        then:
            bytesPool.size() == 2
    }
    
    def "should throw an exception on trying to release a bytebuffer whose size is not equal to defined on constructing pool"()
    {
        given:
            BytesPool bytesPool = BytesPoolFactory.referenceBytesPool(2, 5)
        when:
            bytesPool.releaseBuffer(Bytes.wrapForRead(ByteBuffer.allocateDirect(4)))
        then:
            thrown(IllegalStateException)
        when:
            bytesPool.releaseBuffer(Bytes.wrapForRead(ByteBuffer.allocateDirect(6)))
        then:
            thrown(IllegalStateException)
    }
    
    def "should return correctly sized and configured bytebuffer"()
    {
        given:
            BytesPool bytesPool = BytesPoolFactory.referenceBytesPool(2, 100)
        when:
            Bytes<ByteBuffer> buf = bytesPool.leaseBuffer()
        then:
            buf.capacity() == 100
            buf.readPosition() == 0
            buf.writePosition() == 0
    }
    
}
