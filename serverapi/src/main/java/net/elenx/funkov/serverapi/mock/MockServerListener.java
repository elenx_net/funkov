package net.elenx.funkov.serverapi.mock;

import net.elenx.funkov.serverapi.ConnectionListener;
import net.elenx.funkov.serverapi.ServerListener;

import java.nio.channels.SocketChannel;
import java.util.Optional;
import java.util.function.Function;

public class MockServerListener implements ServerListener
{
    private final Runnable onStartServer;
    private final Runnable onCloseServer;
    private final Function<SocketChannel, Optional<ConnectionListener>> onConnectionAccept;
    private final Runnable afterWarmup;

    @Override
    public void onStartServer()
    {
        if(onStartServer != null)
            onStartServer.run();
    }

    @Override
    public void onCloseServer()
    {
        if(onCloseServer != null)
            onCloseServer.run();
    }

    @Override
    public Optional<ConnectionListener> onConnectionAccept(SocketChannel clientSocket)
    {
        return onConnectionAccept != null ? onConnectionAccept.apply(clientSocket) : Optional.empty();
    }

    @Override
    public void afterWarmup()
    {
        if(afterWarmup != null)
            afterWarmup.run();
    }

    public static class Builder {

        private Runnable onStartServer;
        private Runnable onCloseServer;
        private Function<SocketChannel, Optional<ConnectionListener>> onConnectionAccept;
        private Runnable afterWarmup;

        public Builder onStartServer(Runnable onStartServer)
        {
            this.onStartServer = onStartServer;
            return this;
        }

        public Builder onCloseServer(Runnable onCloseServer)
        {
            this.onCloseServer = onCloseServer;
            return this;
        }

        public Builder onConnectionAccept(Function<SocketChannel, Optional<ConnectionListener>> onConnectionAccept)
        {
            this.onConnectionAccept = onConnectionAccept;
            return this;
        }

        public Builder afterWarmup(Runnable afterWarmup)
        {
            this.afterWarmup = afterWarmup;
            return this;
        }

        public MockServerListener build()
        {
            return new MockServerListener(onStartServer, onCloseServer, onConnectionAccept, afterWarmup);
        }
    }

    public static Builder builder()
    {
        return new Builder();
    }

    private MockServerListener(Runnable onStartServer, Runnable onCloseServer, Function<SocketChannel, Optional<ConnectionListener>> onConnectionAccept,
                               Runnable afterWarmup)
    {
        this.onStartServer = onStartServer;
        this.onCloseServer = onCloseServer;
        this.onConnectionAccept = onConnectionAccept;
        this.afterWarmup = afterWarmup;
    }
}
