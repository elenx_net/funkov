package net.elenx.funkov.serverapi.mock;

public interface Warmup
{

    /**
     * @return Port on which warmup server will start. Should be different than the one passed for real server.
     * It gives you possibility for warmup without accepting real connections which could come during warmup.
     */
    int warmupServerPort();

    /**
     * Put your warmup logic here, server is started on port defined in {@link #warmupServerPort()} with
     * {@link net.elenx.funkov.serverapi.ServerListener} passed in configuration.
     * All resources associated to warmup will be released before starting "real" server.
     */
    void doWarmup();
}
