package net.elenx.funkov.serverapi;

import net.elenx.funkov.common.annotations.ThreadSafeContext;
import net.openhft.chronicle.bytes.Bytes;

import java.nio.ByteBuffer;

/**
 * Connection is attachment of #{@link java.nio.channels.SelectionKey}, objects are also kept in WeakSetReference
 * on server side, so we shouldn't keep their references to allow releasing memory associated with Connection
 */
@ThreadSafeContext
public interface Connection
{
    /**
     * @return Current connection identity number
     */
    int getConnectionId();

    /**
     * @return Native ByteBuffer. Server side doesn't touch this Buffer in any circumstances.
     */
    Bytes<ByteBuffer> associatedByteBuffer();

    /**
     * Closes current connection and removes its #{@link java.nio.channels.SelectionKey} from #{@link java.nio.channels.ServerSocketChannel}
     * You should not keep reference to this object so it can be released. After closing #{@link #associatedByteBuffer() starts returning null}.
     * <p>
     * Connections are kept as WeakReferences, they gets garbage collected soon after closing.
     */
    void close();
}
