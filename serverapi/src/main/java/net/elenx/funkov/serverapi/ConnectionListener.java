package net.elenx.funkov.serverapi;

import net.elenx.funkov.common.annotations.ThreadSafeContext;

import java.nio.ByteBuffer;

/**
 * Connection associated with single client. Each client gets his own instance of ConnectionListener
 */
@ThreadSafeContext
public interface ConnectionListener
{

    /**
     * Method is invoked if exception occurs when access socket for reading or writing operations. It also handles
     * any exceptions thrown within {@link #afterRead(Connection)} and {@link #afterWrite(Connection)} or any
     * of {@link Connection} methods.
     *
     * @param exc        Exception which occurred while handling request / response
     * @param connection Current connection affected by exception
     */
    default void onConnectionError(Exception exc, Connection connection)
    {
    }

    /**
     * This method is invoked if close connection by {@link Connection#close()}, socket reads -1, or errors occurs
     * while reading / writing to / from socket, what generally means the connection is broken. It's also invoked
     * on server instance shutdown
     *
     * @param connection Connection associated with client
     */
    default void onCloseConnection(Connection connection)
    {
    }


    /**
     * This method is invoke just after non blocking read from socket
     *
     * @param connection Connection associated with client
     * @return <code>true</code> if you have read whole message, so we can register for OP_WRITE event
     * or <false>if you weren't able to read whole message, what means we continue listening for OP_READ events</false>
     */
    boolean afterRead(Connection connection);


    /**
     * This method is invoke just after non blocking write to socket.
     *
     * @param connection Connection associated with client
     * @return <code>true</code> if you written whole message so we can register connection for OP_READ event
     * or <code>false</code> if you weren't able to send whole message, what means we continue listening for OP_WRITE events
     */
    default boolean afterWrite(Connection connection)
    {
        ByteBuffer buffer = connection.associatedByteBuffer().underlyingObject();
        var allWritten = buffer.position() == buffer.limit();
        if (allWritten)
            buffer.clear();
        return allWritten;
    }

    /**
     * Invoked just before closing server, all resources will be released after this method invocation.
     */
    default void beforeServerClose()
    {

    }
}
