package net.elenx.funkov.serverapi.mock;

import net.elenx.funkov.serverapi.Connection;
import net.elenx.funkov.serverapi.ConnectionListener;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

public class MockConnectionListener implements ConnectionListener
{
    private final BiConsumer<Exception, Connection> onConnectionError;
    private final Consumer<Connection> onCloseConnection;
    private final Function<Connection, Boolean> afterRead;
    private final Function<Connection, Boolean> afterWrite;
    private final Runnable beforeServerClose;

    @Override
    public void onConnectionError(Exception exc, Connection connection)
    {
        if(onConnectionError != null)
            onConnectionError.accept(exc, connection);
    }

    @Override
    public void onCloseConnection(Connection connection)
    {
        if(onCloseConnection != null)
            onCloseConnection.accept(connection);
    }

    @Override
    public boolean afterRead(Connection connection)
    {
        return afterRead == null || afterRead.apply(connection);

    }

    @Override
    public void beforeServerClose()
    {
        if(beforeServerClose != null)
            beforeServerClose.run();
    }

    @Override
    public boolean afterWrite(Connection connection)
    {
        return afterWrite == null ? ConnectionListener.super.afterWrite(connection) : afterWrite.apply(connection);
    }

    public static MockConnectionListener.Builder builder()
    {
        return new MockConnectionListener.Builder();
    }

    public static class Builder {

        private BiConsumer<Exception, Connection> onConnectionError;
        private Consumer<Connection> onCloseConnection;
        private Function<Connection, Boolean> afterRead;
        private Function<Connection, Boolean> afterWrite;
        private Runnable beforeServerClose;

        public Builder onConnectionError(BiConsumer<Exception, Connection> onConnectionError)
        {
            this.onConnectionError = onConnectionError;
            return this;
        }

        public Builder onCloseConnection(Consumer<Connection> onCloseConnection)
        {
            this.onCloseConnection = onCloseConnection;
            return this;
        }

        public Builder afterRead(Function<Connection, Boolean> afterRead)
        {
            this.afterRead = afterRead;
            return this;
        }

        public Builder afterWrite(Function<Connection, Boolean> afterWrite)
        {
            this.afterWrite = afterWrite;
            return this;
        }

        public Builder beforeServerClose(Runnable beforeServerClose)
        {
            this.beforeServerClose = beforeServerClose;
            return this;
        }

        public MockConnectionListener build()
        {
            return new MockConnectionListener(onConnectionError, onCloseConnection, afterRead, afterWrite, beforeServerClose);
        }
    }

    private MockConnectionListener(BiConsumer<Exception, Connection> onConnectionError, Consumer<Connection> onCloseConnection,
                                   Function<Connection, Boolean> afterRead, Function<Connection, Boolean> afterWrite,
                                   Runnable beforeServerClose)
    {
        this.onConnectionError = onConnectionError;
        this.onCloseConnection = onCloseConnection;
        this.afterRead = afterRead;
        this.afterWrite = afterWrite;
        this.beforeServerClose = beforeServerClose;
    }
}
