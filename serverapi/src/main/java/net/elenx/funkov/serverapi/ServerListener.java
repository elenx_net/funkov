package net.elenx.funkov.serverapi;

import net.elenx.funkov.common.annotations.ThreadSafeContext;

import java.nio.channels.SocketChannel;
import java.util.Optional;

@ThreadSafeContext
public interface ServerListener
{
    /**
     * Invoked after warmup (if configured), when server if fully initialized just before Selector loop
     */
    default void onStartServer()
    {
    }

    /**
     * Invoked just before closing server, all resources are released, the server channel is also closed.
     */
    default void onCloseServer()
    {
    }

    /**
     * @param clientSocket Clients' {@link SocketChannel} identifying incoming connection
     * @return Listener which is used for handling further requests within single client connection
     * If Optional is empty, connection will be rejected.
     */
    Optional<ConnectionListener> onConnectionAccept(SocketChannel clientSocket);


    /**
     * Invoked after warmup (if configured). You can clean any resources here used during warmup
     */
    default void afterWarmup()
    {
    }
}
