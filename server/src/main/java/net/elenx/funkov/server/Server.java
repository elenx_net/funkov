package net.elenx.funkov.server;

import net.elenx.funkov.server.configuration.FunkovServerConfiguration;
import net.elenx.funkov.server.reactor.Reactor;
import net.elenx.funkov.serverapi.ServerListener;

public interface Server
{
    /**
     * starts server and then block current thread
     */
    void start();

    /**
     * starts server on another thread, this method returns when server is fully initialized
     */
    void startOnNewThread();

    static Server server(FunkovServerConfiguration configuration, ServerListener listener)
    {
        return new Reactor(configuration, listener);
    }

    static Server server(ServerListener listener)
    {
        return new Reactor(FunkovServerConfiguration.DEFAULT_CONFIGURATION, listener);
    }

    /**
     * @return does server is currently running main loop
     */
    boolean isRunning();

    /**
     * Closes server, removes all connections and releases all resources.
     */
    void close();
}
