package net.elenx.funkov.server.reactor;

import net.elenx.funkov.common.annotations.ThreadSafeContext;
import net.elenx.funkov.server.connection.FunkovConnection;
import net.elenx.funkov.serverapi.ServerListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

/**
 * Acceptor is responsible for accepting / rejecting new incoming connections
 */
@ThreadSafeContext
public class Acceptor
{
    private static final Logger log = LoggerFactory.getLogger(Acceptor.class);

    private Acceptor()
    {
    }

    /**
     * @return New connection if not rejected by {@link net.elenx.funkov.serverapi.ServerListener#onConnectionAccept(SocketChannel)}}
     * or <code>null</code>
     * Returned connection is associated with server Selector, initially with OP_READ flag
     */
    public static FunkovConnection accept(SelectionKey selectionKey, ServerListener serverListener)
    {
        try
        {
            ServerSocketChannel serverChannel = (ServerSocketChannel) selectionKey.channel();
            SocketChannel clientChannel = serverChannel.accept();

            if (clientChannel == null)
            {
                log.warn("Connection has been lost");
                return null;
            }

            var maybeConnection = serverListener.onConnectionAccept(clientChannel);
            if (maybeConnection.isPresent())
            {
                var connectionListener = maybeConnection.get();

                Selector selector = selectionKey.selector();
                clientChannel.configureBlocking(false);
                clientChannel.register(selector, SelectionKey.OP_READ);
                SelectionKey channelKey = clientChannel.keyFor(selector);

                FunkovConnection connection = new FunkovConnection(clientChannel, channelKey, connectionListener);
                channelKey.attach(connection);

                log.info("Accepted new connection ID: {}", connection.getConnectionId());
                return connection;
            } else {
                return null;
            }
        } catch (Exception e)
        {
            log.error("Error on accepting connection. Server is going to continue but this connection was refused", e);
            return null;
        }
    }
}
