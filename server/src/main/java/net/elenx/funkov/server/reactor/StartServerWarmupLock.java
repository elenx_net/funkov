package net.elenx.funkov.server.reactor;

import java.util.concurrent.Semaphore;

import static net.elenx.funkov.common.SneakyThrow.sneakyThrow;

public class StartServerWarmupLock
{
    private final Semaphore semaphore = new Semaphore(1);

    private final String warmupThreadName;

    public StartServerWarmupLock(String warmupThreadName)
    {
        this.warmupThreadName = warmupThreadName;
    }

    public void lock(Thread thread)
    {
        if(!thread.getName().equals(warmupThreadName))
        {
            try
            {
                semaphore.acquire();
            } catch (InterruptedException e)
            {
                sneakyThrow(e);
                Thread.currentThread().interrupt();
            }
        }
    }

    public void unlock()
    {
        semaphore.release();
    }
}
