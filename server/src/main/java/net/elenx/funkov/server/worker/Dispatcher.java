package net.elenx.funkov.server.worker;

import net.elenx.funkov.server.connection.FunkovConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Dispatcher
{
    private static final Logger log = LoggerFactory.getLogger(Dispatcher.class);

    private final WorkerThread[] workers;

    public Dispatcher(int workerThreads)
    {
        workers = new WorkerThread[workerThreads];
    }

    public void start()
    {
        for (int i = 0; i < workers.length; i++)
        {
            WorkerThread workerThread = new WorkerThread(new Worker(), new String("Worker " + i));
            workers[i] = workerThread;
            workerThread.start();
            while (!workerThread.worker.notClosed()) ;
        }
    }

    public void dispatch(FunkovConnection connection)
    {
        int workerIndex = connection.getConnectionId() % workers.length;
        workers[workerIndex].worker.submit(connection);

        log.trace("Connection dispatched to worker {}", workerIndex);
    }

    public void shutdown()
    {
        for (WorkerThread workerThread : workers)
        {
            workerThread.worker.shutdown();
        }
        waitUntilWorkersFinish();
    }

    public void shutdownForce()
    {
        for (WorkerThread workerThread : workers)
        {
            workerThread.worker.shutdownForce();
        }

        waitUntilWorkersFinish();

    }

    private void waitUntilWorkersFinish()
    {
        for (WorkerThread workerThread : workers)
        {
            try
            {
                workerThread.join();
            } catch (InterruptedException e)
            {
                log.error("", e);
                Thread.currentThread().interrupt();
            }
        }
    }
}
