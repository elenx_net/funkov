package net.elenx.funkov.server.warmup;

import net.elenx.funkov.client.WebSocketClient;
import net.elenx.funkov.client.WebSocketClientFactory;
import net.elenx.funkov.client.WebSocketClientListener;
import net.elenx.funkov.model.Handshake;
import net.elenx.funkov.serverapi.mock.Warmup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.util.concurrent.CompletableFuture;

public class WebSocketServerWarmup implements Warmup
{
    private static final Logger log = LoggerFactory.getLogger(WebSocketServerWarmup.class);
    private static final int WARMUP_PORT = 5556;

    private final int iterations;
    private final boolean exitProcessAfterWarmup;


    public WebSocketServerWarmup(int iterations, boolean exitProcessAfterWarmup)
    {
        this.iterations = iterations;
        this.exitProcessAfterWarmup = exitProcessAfterWarmup;
    }

    public WebSocketServerWarmup(int iterations)
    {
        this.iterations = iterations;
        this.exitProcessAfterWarmup = false;
    }

    @Override
    public int warmupServerPort()
    {
        return WARMUP_PORT;
    }

    @Override
    public void doWarmup()
    {
        try
        {
            log.info("Starting {} warmup iterations", iterations);

            URI serverUri = new URI("ws://127.0.0.1:" + WARMUP_PORT);
            WebSocketClient client = WebSocketClientFactory.nettyClient(serverUri.toString());
            for (int i = 0; i < iterations; i++)
            {
                CompletableFuture<Void> onHandshake = new CompletableFuture<>();
                client.connect(createListener(onHandshake));
            }
            client.close();

        } catch (Exception e)
        {
            log.error("Exception during warmup, closing application", e);
            System.exit(1);
        }

        if (exitProcessAfterWarmup)
        {
            log.info("Warmup finished, closing process");
            System.exit(0);
        }
    }

    private static WebSocketClientListener createListener(CompletableFuture<Void> onHandshake)
    {
        return new WebSocketClientListener()
        {
            @Override
            public void onConnect(Handshake handshake)
            {
                onHandshake.complete(null);
            }
        };
    }

}
