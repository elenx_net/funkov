package net.elenx.funkov.server.configuration;

import net.elenx.funkov.serverapi.mock.Warmup;

public record FunkovServerConfiguration(int port, int workerThreads, boolean forceShutdown, Warmup warmup)
{
    public static final FunkovServerConfiguration DEFAULT_CONFIGURATION = FunkovServerConfiguration.builder().build();

    public static class Builder
    {
        private int port = 5555;
        private int workerThreads = 1;
        private boolean forceShutdown;
        private Warmup warmup;

        public Builder port(int port)
        {
            this.port = port;
            return this;
        }

        public Builder workerThreads(int workerThreads)
        {
            this.workerThreads = workerThreads;
            return this;
        }

        public Builder forceShutdown(boolean forceShutdown)
        {
            this.forceShutdown = forceShutdown;
            return this;
        }

        public Builder warmup(Warmup warmup)
        {
            this.warmup = warmup;
            return this;
        }

        public FunkovServerConfiguration build()
        {
            this.workerThreads = workerThreads <= 0 ? 1 : workerThreads;
            return new FunkovServerConfiguration(port, workerThreads, forceShutdown, warmup);
        }
    }

    public static Builder builder()
    {
        return new Builder();
    }

    public FunkovServerConfiguration withPort(int port)
    {
        return new FunkovServerConfiguration(port, workerThreads, forceShutdown, warmup);
    }

}
