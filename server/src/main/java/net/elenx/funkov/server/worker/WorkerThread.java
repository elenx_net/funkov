package net.elenx.funkov.server.worker;

import net.elenx.funkov.common.thread.CleaningThread;

public class WorkerThread extends CleaningThread
{
    protected final Worker worker;

    public WorkerThread(Worker worker, String name)
    {
        super(worker, name);
        this.worker = worker;
    }
}
