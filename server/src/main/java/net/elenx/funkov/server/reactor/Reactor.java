package net.elenx.funkov.server.reactor;

import net.elenx.funkov.common.annotations.SynchronizationRequired;
import net.elenx.funkov.common.properties.Properties;
import net.elenx.funkov.server.Server;
import net.elenx.funkov.server.configuration.FunkovServerConfiguration;
import net.elenx.funkov.server.connection.FunkovConnection;
import net.elenx.funkov.server.worker.Dispatcher;
import net.elenx.funkov.serverapi.ServerListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.ClosedSelectorException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;
import java.util.WeakHashMap;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.LockSupport;

import static net.elenx.funkov.common.SneakyThrow.sneakyThrow;

@SynchronizationRequired
public class Reactor implements Server
{
    private static final String WARMUP_THREAD_NAME = "WARMUP_THREAD";

    private static final Logger log = LoggerFactory.getLogger(Reactor.class);
    private static boolean firstRun = true;

    // static to avoid already bind exception when started by multiple threads in multiple instances
    private static final StartServerWarmupLock STARTING_LOCK = new StartServerWarmupLock(WARMUP_THREAD_NAME);
    private final AtomicBoolean isRunning = new AtomicBoolean();
    private volatile boolean closed = true;

    private final FunkovServerConfiguration configuration;
    private final Dispatcher dispatcher;
    private final ServerListener serverListener;
    private final Set<FunkovConnection> connections = Collections.newSetFromMap(new WeakHashMap<>());

    private Selector selector;
    private ServerSocketChannel serverSocket;

    private Thread reactorThread;

    public Reactor(FunkovServerConfiguration configuration, ServerListener listener)
    {
        Properties.put("workersThreads", configuration.workerThreads());
        this.configuration = configuration;
        this.dispatcher = new Dispatcher(configuration.workerThreads());
        this.serverListener = listener;
    }

    @Override
    public void start()
    {
        start(null);
    }

    private void start(Thread threadWaitingForStart)
    {
        boolean runLoop = false;
        STARTING_LOCK.lock(Thread.currentThread());
        try
        {
            if(!closed)
            {
                log.warn("Server is running, call close() before running again");
                return;
            }
            runLoop = true;
            reactorThread = Thread.currentThread();
            reactorThread.setName("Reactor Thread " + UUID.randomUUID());
            startServer();
            serverListener.onStartServer();
            isRunning.set(true);
            closed = false;

        } catch (Exception e)
        {
            log.error("", e);
            closeReactor();
        }
        finally
        {
            if(threadWaitingForStart != null)
                LockSupport.unpark(threadWaitingForStart);
            STARTING_LOCK.unlock();
        }

        if(runLoop)
            runLoop();
    }

    private void startServer()
    {
        try
        {
            if (configuration.warmup() != null && firstRun)
            {
                firstRun = false;
                runWarmup();
            }

            log.info("Starting dispatcher, workers = {}", configuration.workerThreads());
            dispatcher.start();
            log.info("Dispatcher started");

            selector = Selector.open();
            serverSocket = ServerSocketChannel.open();

            serverSocket.bind(new InetSocketAddress(configuration.port()));
            serverSocket.configureBlocking(false);
            serverSocket.register(selector, SelectionKey.OP_ACCEPT);

            log.info("Server started at port {}", configuration.port());

        } catch (Exception e)
        {
            sneakyThrow(e);
        }
    }

    private void runWarmup() throws InterruptedException
    {
        log.info("Starting warmup...");
        Server server = Server.server(configuration.withPort(configuration.warmup().warmupServerPort()), serverListener);
        Thread thread = new Thread(server::start);
        thread.setName(WARMUP_THREAD_NAME);
        thread.start();
        while(!server.isRunning());

        CompletableFuture
                .runAsync(() -> configuration.warmup().doWarmup())
                .thenAccept(ignore -> server.close());

        thread.join();
        serverListener.afterWarmup();
        log.info("Warmup done");
    }

    private void runLoop()
    {
        try
        {
            while (isRunning.get())
            {
                selector.select();
                Iterator<SelectionKey> selectionKeys = selector.selectedKeys().iterator();
                handleSelectionKeys(selectionKeys);
            }
            closeReactor();
        } catch (ClosedSelectorException ignored)
        {
            // intentionally omitted handling, close was requested
        } catch (IOException ex)
        {
            log.error("", ex);
            close();
        }
    }

    private void handleSelectionKeys(Iterator<SelectionKey> selectionKeys)
    {
        try
        {
            while (selectionKeys.hasNext())
            {
                SelectionKey sk = selectionKeys.next();
                selectionKeys.remove();

                if (!sk.isValid())
                    continue;

                if (sk.isAcceptable())
                {
                    var connection = Acceptor.accept(sk, serverListener);
                    if(connection != null)
                        connections.add(connection);
                }
                else
                {
                    sk.interestOps(0);
                    dispatcher.dispatch((FunkovConnection) sk.attachment());
                }
            }
        } catch (Exception ex)
        {
            log.error("", ex);
        }
    }

    @Override
    public boolean isRunning()
    {
        return isRunning.get();
    }

    @Override
    public void close()
    {
        if(isRunning.getAndSet(false))
        {
            while(!closed)
            {
                Thread.onSpinWait();
                selector.wakeup();
                LockSupport.parkUntil(System.currentTimeMillis() + 10L);
            }
        }
    }

    private void closeReactor()
    {
        if (Thread.currentThread() != reactorThread)
            throw new IllegalStateException("Closing reactor somehow happens not within its thread");

        connections.forEach(connection ->
        {
            connection.serverCloseRequest();
            dispatcher.dispatch(connection);
        });

        log.info("Shutdowning dispatcher");
        shutdownDispatcher();
        connections.clear();
        log.info("Closing server socket");
        closeServerSocket();
        serverListener.onCloseServer();
        closed = true;

        log.info("Server closed");
    }

    private void shutdownDispatcher()
    {
        if (configuration.forceShutdown())
            dispatcher.shutdownForce();
        else
            dispatcher.shutdown();
    }

    private void closeServerSocket()
    {
        try
        {
            if (selector != null)
                selector.close();

            if (serverSocket != null)
                serverSocket.close();
        } catch (Exception e)
        {
            sneakyThrow(e);
        }
    }

    @Override
    public void startOnNewThread()
    {
        var askingThread = Thread.currentThread();
        new Thread(() -> start(askingThread)).start();
        LockSupport.park();
    }
}
