package net.elenx.funkov.server;

import net.elenx.funkov.common.properties.Properties;
import net.elenx.funkov.protocol.websocket.WebSocketConnectionListener;
import net.elenx.funkov.protocol.websocket.WebsocketProtocol;
import net.elenx.funkov.server.cli.ServerProgramArguments;
import net.elenx.funkov.server.configuration.FunkovServerConfiguration;
import net.elenx.funkov.server.warmup.WebSocketServerWarmup;

import java.nio.file.Paths;

public class Main
{
    public static void main(String[] args)
    {
        disableIllegalReflectiveAccessWarning();
        var programArguments = ServerProgramArguments.initialize(args);
        Properties.load(Paths.get("app.config"));

        int port = Properties.get("server.port", 5555, int.class);
        int threads = Properties.get("server.workerThreads", Runtime.getRuntime().availableProcessors(), int.class);
        var warmup = programArguments.getIterations() > 0 ? new WebSocketServerWarmup(programArguments.getIterations()) : null;

        var configuration = FunkovServerConfiguration.builder()
                .workerThreads(threads)
                .warmup(warmup)
                .port(port)
                .build();

        var server = Server.server(configuration, new WebsocketProtocol(new MockedWebSocketConnectionListener()));
        server.start();
    }

    private static class MockedWebSocketConnectionListener implements WebSocketConnectionListener
    {

    }

    // https://stackoverflow.com/questions/46454995/how-to-hide-warning-illegal-reflective-access-in-java-9-without-jvm-argument
    public static void disableIllegalReflectiveAccessWarning() {
        System.err.close();
        System.setErr(System.out);
    }
}
