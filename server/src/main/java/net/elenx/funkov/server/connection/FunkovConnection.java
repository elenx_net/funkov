package net.elenx.funkov.server.connection;

import net.elenx.funkov.common.OS;
import net.elenx.funkov.common.annotations.ThreadSafeContext;
import net.elenx.funkov.common.bytes.BytesPool;
import net.elenx.funkov.common.bytes.BytesPoolFactory;
import net.elenx.funkov.common.bytes.BytesToSocket;
import net.elenx.funkov.common.properties.Properties;
import net.elenx.funkov.common.thread.CleaningThreadLocal;
import net.elenx.funkov.serverapi.Connection;
import net.elenx.funkov.serverapi.ConnectionListener;
import net.openhft.chronicle.bytes.Bytes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

@ThreadSafeContext
public class FunkovConnection implements Connection, Closeable, Runnable
{
    private static final Logger log = LoggerFactory.getLogger(FunkovConnection.class);

    private static final int MAX_POOL_SIZE = Properties.get("bytesPool.maxSize", 100_000, int.class);
    private static final int WORKER_THREADS = Properties.get("workersThreads", OS.availableLogicalProcessors() / 2, int.class);
    private static final CleaningThreadLocal<BytesPool> bytesPool;

    static
    {
        bytesPool = CleaningThreadLocal.withCleanup(
                () -> BytesPoolFactory.referenceBytesPool(
                        MAX_POOL_SIZE / (WORKER_THREADS == 0 ? 1 : WORKER_THREADS),
                        OS.memory().pageSize() + OS.tcpBufferSize()),
                BytesPool::clean);

    }

    private static final byte READING = 0;
    private static final byte WRITING = 1;
    private static final byte CLOSING = 2;
    private volatile byte state = READING;

    private boolean open = true;

    private final int connectionId;
    private final SocketChannel clientSocket;
    private final SelectionKey selectionKey;
    private final ConnectionListener connectionListener;
    private Bytes<ByteBuffer> associatedByteBuffer;
    private boolean receivedCloseServerRequest;

    public FunkovConnection(SocketChannel clientChannel, SelectionKey key, ConnectionListener connectionListener)
    {
        this.connectionId = FunkovIdGenerator.generateId();
        this.clientSocket = clientChannel;
        this.selectionKey = key;
        this.connectionListener = connectionListener;
    }

    @Override
    public int getConnectionId()
    {
        return connectionId;
    }

    @Override
    public Bytes<ByteBuffer> associatedByteBuffer()
    {
        return associatedByteBuffer;
    }

    @Override
    public void close()
    {
        try
        {
            // connection is keep in WeakReferenceSet, so it may be already closed but not GC'ed yet
            if (open)
            {
                log.debug("Closing connection ID: {}", connectionId);
                open = false;
                selectionKey.cancel();
                connectionListener.onCloseConnection(this);

                if (receivedCloseServerRequest)
                    connectionListener.beforeServerClose();
            }
        } catch (Exception e)
        {
            log.error("", e);
            invokeOnConnectionError(e);

        } finally
        {
            releasePool();
        }
    }

    private void releasePool()
    {
        if (associatedByteBuffer != null)
        {
            bytesPool.get().releaseBuffer(associatedByteBuffer);

            // make sure it can't be used after release
            associatedByteBuffer = null;
        }
    }

    @Override
    public void run()
    {
        if (open)
        {
            try
            {
                if (associatedByteBuffer == null)
                    associatedByteBuffer = bytesPool.get().leaseBuffer();

                switch (state)
                {
                    case READING -> read();
                    case WRITING -> write();
                    case CLOSING -> close();
                    default -> throw new IllegalStateException("Unexpected value: " + state);
                }

            } catch (CancelledKeyException keyException)
            {
                // ignore
            } catch (Exception e)
            {
                log.error("", e);
                invokeOnConnectionError(e);
                close();
            }
        }
    }

    private void read() throws IOException
    {
        int bytesRead = BytesToSocket.read(associatedByteBuffer, clientSocket);

        if (bytesRead == -1)
        {
            close();
            return;
        }

        boolean readFinished = invokeAfterReadListener();
        if (readFinished)
        {
            // Try to immediately write response, with a bit of luck we can avoid registering OP_WRITE
            BytesToSocket.write(associatedByteBuffer, clientSocket);
            boolean writeFinished = invokeAfterWriteListener();
            if (writeFinished)
            {
                selectionKey.interestOps(SelectionKey.OP_READ);
            } else
            {
                selectionKey.interestOps(SelectionKey.OP_WRITE);
                state = WRITING;
            }
        } else
        {
            if (selectionKey.isValid())
                selectionKey.interestOps(SelectionKey.OP_READ);
        }
        selectionKey.selector().wakeup();
    }

    private void write() throws IOException
    {
        BytesToSocket.write(associatedByteBuffer, clientSocket);
        boolean writeFinished = invokeAfterWriteListener();
        if (writeFinished)
        {
            selectionKey.interestOps(SelectionKey.OP_READ);
            state = READING;
        } else
        {
            if (selectionKey.isValid())
                selectionKey.interestOps(SelectionKey.OP_WRITE);
        }
        selectionKey.selector().wakeup();
    }

    // May be requested by reactor thread : <
    public void serverCloseRequest()
    {
        // writes before volatile store are also visible to other threads, do not change instruction order
        receivedCloseServerRequest = true;
        this.state = CLOSING;
    }

    private boolean invokeAfterReadListener()
    {
        try
        {
            return connectionListener.afterRead(this);
        } catch (Exception e)
        {
            invokeOnConnectionError(e);
            return false;
        }
    }

    private boolean invokeAfterWriteListener()
    {
        try
        {
            return connectionListener.afterWrite(this);
        } catch (Exception e)
        {
            invokeOnConnectionError(e);
            return false;
        }
    }

    private void invokeOnConnectionError(Exception e)
    {
        try
        {
            connectionListener.onConnectionError(e, this);
        } catch (Exception exc)
        {
            log.error("", exc);
        }
    }
}
