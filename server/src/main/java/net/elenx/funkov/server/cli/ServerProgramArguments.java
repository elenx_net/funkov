package net.elenx.funkov.server.cli;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class ServerProgramArguments
{
    private final int iterations;

    private ServerProgramArguments(int iterations)
    {
        this.iterations = iterations;
    }

    public static ServerProgramArguments initialize(String... args)
    {
        Options options = new Options();
        options.addOption(Option.builder("i")
                .longOpt("iterations")
                .hasArg(true)
                .desc("warmup iterations, integer value")
                .required(false)
                .build());

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd;
        try {
            cmd = parser.parse(options, args);
            return cmd.hasOption("i") ? new ServerProgramArguments(Integer.parseInt(cmd.getOptionValue("i"))) : new ServerProgramArguments(0);
        } catch (ParseException  | NumberFormatException exc) {
            System.out.println("Error parsing command-line arguments!");
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp( "java -jar --enable-preview server-$version.jar [-i N]", options );
            System.exit(1);
            return null;
        }
    }

    public int getIterations()
    {
        return iterations;
    }
}
