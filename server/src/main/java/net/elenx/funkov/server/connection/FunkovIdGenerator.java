package net.elenx.funkov.server.connection;

import net.elenx.funkov.common.annotations.SynchronizationRequired;

@SynchronizationRequired
public class FunkovIdGenerator
{
    private static int counter;

    private FunkovIdGenerator() {}

    public static int generateId() {
        return ++counter;
    }
}
