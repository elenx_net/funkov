package net.elenx.funkov.server.reactor

import net.elenx.funkov.common.TestExpectedException
import net.elenx.funkov.common.handler.BooleanHandler
import net.elenx.funkov.common.handler.Handlers
import net.elenx.funkov.server.Server
import net.elenx.funkov.server.configuration.FunkovServerConfiguration
import net.elenx.funkov.serverapi.ConnectionListener
import net.elenx.funkov.serverapi.ServerListener
import net.elenx.funkov.serverapi.mock.MockConnectionListener
import net.elenx.funkov.serverapi.mock.MockServerListener
import spock.lang.Specification

class ConnectionListenerTest extends Specification
{
    private static final port = 5555
    private static final FunkovServerConfiguration CONFIGURATION = FunkovServerConfiguration.builder()
            .port(port)
            .build()
    
    def "should invoke afterRead() after reading from socket"()
    {
        given:
            BooleanHandler handler = Handlers.handler()
            ConnectionListener connectionListener = MockConnectionListener.builder()
                    .afterRead({ handler.invoke(); true }).build()
            
            ServerListener listener = listener(connectionListener)
            Server server = Server.server(CONFIGURATION, listener)
            server.startOnNewThread()
            Socket clientSocket = new Socket("127.0.0.1", port)
        when:
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true)
            out.println("asd")
        then:
            handler.invoked()
        cleanup:
            server.close()
    }
    
    def "should invoke afterWrite() after writing to socket"()
    {
        given:
            BooleanHandler handler = Handlers.handler()
            ConnectionListener connectionListener = MockConnectionListener.builder()
                    .afterRead({ true })
                    .afterWrite({ handler.invoke(); true })
                    .build()
            
            ServerListener listener = listener(connectionListener)
            Server server = Server.server(CONFIGURATION, listener)
            server.startOnNewThread()
            Socket clientSocket = new Socket("127.0.0.1", port)
        when:
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true)
            out.println("asd")
        then:
            handler.invoked()
        cleanup:
            server.close()
    }
    
    def "should invoke onCloseConnection() when client close his socket"()
    {
        given:
            BooleanHandler handler = Handlers.handler()
            ConnectionListener connectionListener = MockConnectionListener.builder()
                    .onCloseConnection({ handler.invoke() })
                    .build()
            
            ServerListener listener = listener(connectionListener)
            Server server = Server.server(CONFIGURATION, listener)
            server.startOnNewThread()
            Socket clientSocket = new Socket("127.0.0.1", port)
        when:
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true)
            out.println("asd")
            clientSocket.close()
        then:
            handler.invoked()
        cleanup:
            server.close()
    }
    
    def "should invoke onCloseConnection() when listener close connection"()
    {
        given:
            BooleanHandler handler = Handlers.handler()
            ConnectionListener connectionListener = MockConnectionListener.builder()
                    .afterRead({ it.close(); true })
                    .onCloseConnection({ handler.invoke() })
                    .build()
            
            ServerListener listener = listener(connectionListener)
            Server server = Server.server(CONFIGURATION, listener)
            server.startOnNewThread()
            Socket clientSocket = new Socket("127.0.0.1", port)
        when:
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true)
            out.println("asd")
        then:
            handler.invoked()
        cleanup:
            server.close()
    }
    
    def "should invoke onCloseConnection() on closing server"()
    {
        given:
            BooleanHandler handler = Handlers.handler()
            ConnectionListener connectionListener = MockConnectionListener.builder()
                    .onCloseConnection({ handler.invoke() })
                    .build()
            
            ServerListener listener = listener(connectionListener)
            Server server = Server.server(CONFIGURATION, listener)
            server.startOnNewThread()
        when:
            Socket clientSocket = new Socket("127.0.0.1", port)
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true)
            out.println("asd")
        then:
            // wait until client connects
            sleep(100)
            server.close()
            handler.invoked()
    }
    
    def "should invoke onConnectionError() if error occurs on closing socket client connection"()
    {
        given:
            BooleanHandler handler = Handlers.handler()
            ConnectionListener connectionListener = MockConnectionListener.builder()
                    .onConnectionError((k, v) -> { handler.invoke() })
                    .onCloseConnection({ throw new TestExpectedException() })
                    .build()
            
            ServerListener listener = listener(connectionListener)
            Server server = Server.server(CONFIGURATION, listener)
            server.startOnNewThread()
            Socket clientSocket = new Socket("127.0.0.1", port)
        when:
            clientSocket.close()
        then:
            handler.invoked()
        cleanup:
            server.close()
    }
    
    
    def "should invoke onConnectionError() if error occurs on closing connection in afterRead() method"()
    {
        given:
            BooleanHandler handler = Handlers.handler()
            ConnectionListener connectionListener = MockConnectionListener.builder()
                    .onCloseConnection({ throw new TestExpectedException() })
                    .onConnectionError((k, v) -> { handler.invoke() })
                    .afterRead({ it.close(); true })
                    .build()
            
            ServerListener listener = listener(connectionListener)
            Server server = Server.server(CONFIGURATION, listener)
            server.startOnNewThread()
            Socket clientSocket = new Socket("127.0.0.1", port)
        when:
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true)
            out.println("asd")
        then:
            handler.invoked()
        cleanup:
            server.close()
    }
    
    def "should invoke onConnectionError() if error occurs on closing connection in afterWrite() method"()
    {
        given:
            BooleanHandler handler = Handlers.handler()
            ConnectionListener connectionListener = MockConnectionListener.builder()
                    .onCloseConnection({ throw new TestExpectedException() })
                    .onConnectionError((exc, con) -> { handler.invoke() })
                    .afterWrite({ it.close(); true })
                    .build()
            
            ServerListener listener = listener(connectionListener)
            Server server = Server.server(CONFIGURATION, listener)
            server.startOnNewThread()
            Socket clientSocket = new Socket("127.0.0.1", port)
        when:
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true)
            out.println("asd")
        then:
            handler.invoked()
        cleanup:
            server.close()
    }
    
    def "should invoke beforeServerClose() listener just before closing server"()
    {
        given:
            BooleanHandler handler = Handlers.handler()
            ConnectionListener connectionListener = MockConnectionListener.builder()
                    .beforeServerClose({ handler.invoke() })
                    .build()
            ServerListener listener = listener(connectionListener)
            Server server = Server.server(CONFIGURATION, listener)
        when:
            server.startOnNewThread()
            Socket clientSocket = new Socket("127.0.0.1", port)
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true)
            out.println("asd")
            // wait until client connects
            sleep(100)
            server.close()
        then:
            handler.invoked()
    }
    
    
    private static ServerListener listener(ConnectionListener connectionListener)
    {
        return MockServerListener.builder()
                .onConnectionAccept({ Optional.of(connectionListener) })
                .build()
    }
}
