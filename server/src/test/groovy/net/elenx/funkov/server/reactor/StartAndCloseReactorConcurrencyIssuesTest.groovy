package net.elenx.funkov.server.reactor

import net.elenx.funkov.common.TestExpectedException
import net.elenx.funkov.server.Server
import net.elenx.funkov.server.configuration.FunkovServerConfiguration
import net.elenx.funkov.serverapi.mock.MockServerListener
import spock.lang.Specification

class StartAndCloseReactorConcurrencyIssuesTest extends Specification
{
    private static FunkovServerConfiguration configuration = FunkovServerConfiguration.builder()
            .port(5555)
            .workerThreads(1)
            .build()
    
    def "closing already closed server has no effect"()
    {
        given:
            Server server = Server.server(configuration, MockServerListener.builder().build())
        when:
            server.close()
        then:
            !server.isRunning()
    }
    
    def "Starting already running server has no effect"()
    {
        given:
            Server server = Server.server(configuration, MockServerListener.builder().build())
            server.startOnNewThread()
        when:
            server.start()
        then:
            server.running
        cleanup:
            server.close()
    }
    
    def "calling close on currently closing server has no effect"()
    {
        given:
            MockServerListener listener = MockServerListener.builder()
                    .onCloseServer({ Thread.sleep(1000) })
                    .build()
            Server server = Server.server(configuration, listener)
            server.startOnNewThread()
        when:
            server.close()
            server.close()
        then:
            !server.running
    }
    
    def "race condition should be correctly handled when trying to run server multiple times"()
    {
        given:
            MockServerListener listener = MockServerListener.builder()
                    .build()
            Server server = Server.server(configuration, listener)
        when:
            new Thread({ server.start() }).start()
            new Thread({ server.start() }).start()
            new Thread({ server.start() }).start()
        then:
            Thread.sleep(1200)
            noExceptionThrown()
        cleanup:
            server.close()
    }
    
    def "race condition should be correctly handled when trying to close server multiple times"()
    {
        given:
            MockServerListener listener = MockServerListener.builder()
                    .build()
            Server server = Server.server(configuration, listener)
            server.startOnNewThread()
        when:
            new Thread({ server.close() }).start()
            new Thread({ server.close() }).start()
            new Thread({ server.close() }).start()
        then:
            Thread.sleep(1200)
            !server.running
            noExceptionThrown()
        cleanup:
            server.close()
    }
    
    def "should stop waiting for server if exception is thrown during initialization"()
    {
        given:
            MockServerListener listener = MockServerListener.builder()
                    .onStartServer({ throw new TestExpectedException() })
                    .build()
            Server server = Server.server(configuration, listener)
            server.startOnNewThread()
        expect:
            !server.running
        cleanup:
            server.close()
    }
}