package net.elenx.funkov.server.reactor

import net.elenx.funkov.common.TestExpectedException
import net.elenx.funkov.server.connection.FunkovConnection
import net.elenx.funkov.serverapi.ConnectionListener
import spock.lang.Specification

import java.nio.ByteBuffer
import java.nio.channels.SelectableChannel
import java.nio.channels.SelectionKey
import java.nio.channels.Selector
import java.nio.channels.SocketChannel
import java.nio.channels.spi.SelectorProvider

class FunkovConnectionTest extends Specification
{
    def "should not invoke onConnectionClose twice if already closed"()
    {
        given:
            ConnectionListener connectionListener = Mock()
            FunkovConnection funkovConnection = new FunkovConnection(new MockSocketChannel(false, false), new MockKey(), connectionListener)
        when:
            funkovConnection.close()
            funkovConnection.close()
        then:
            1 * connectionListener.onCloseConnection(_)
    }
    
    def "should close connection when error occurs on reading from socket"()
    {
        given:
            ConnectionListener connectionListener = Mock()
            FunkovConnection funkovConnection = new FunkovConnection(new MockSocketChannel(true, false), new MockKey(), connectionListener)
        when:
            funkovConnection.run()
        then:
            1 * connectionListener.onCloseConnection(_)
            1 * connectionListener.onConnectionError(_,_)
    }
    
    def "should close connection when error occurs on writing to socket"()
    {
        given:
            ConnectionListener connectionListener = Mock()
            connectionListener.afterRead(_) >> true
            FunkovConnection funkovConnection = new FunkovConnection(new MockSocketChannel(false, true), new MockKey(), connectionListener)
        when:
            funkovConnection.run()
        then:
            1 * connectionListener.onCloseConnection(_)
            1 * connectionListener.onConnectionError(_,_)
    }
    
    def "should close connection on next run after close request"()
    {
        given:
            ConnectionListener connectionListener = Mock()
            FunkovConnection funkovConnection = new FunkovConnection(new MockSocketChannel(false, false), new MockKey(), connectionListener)
        when:
            funkovConnection.serverCloseRequest()
            funkovConnection.close()
        then:
            1 * connectionListener.onCloseConnection(_)
    }
    
    def "should not close connection if exception occurs in afterRead() listener"()
    {
        given:
            ConnectionListener connectionListener = Mock()
            connectionListener.afterRead(_) >> { throw new TestExpectedException() }
            FunkovConnection funkovConnection = new FunkovConnection(new MockSocketChannel(false, false), new MockKey(), connectionListener)
        when:
           funkovConnection.run()
        then:
            0 * connectionListener.onCloseConnection(_)
            1 * connectionListener.onConnectionError(_, _)
    }
    
    def "should not close connection if exception occurs in afterWrite() listener"()
    {
        given:
            ConnectionListener connectionListener = Mock()
            connectionListener.afterRead(_) >> true
            connectionListener.afterWrite(_) >> { throw new TestExpectedException() }
            FunkovConnection funkovConnection = new FunkovConnection(new MockSocketChannel(false, false), new MockKey(), connectionListener)
        when:
            funkovConnection.run()
        then:
            0 * connectionListener.onCloseConnection(_)
            1 * connectionListener.onConnectionError(_, _)
    }
    
    def "byte buffer should be null after closing connection"()
    {
        given:
            ConnectionListener connectionListener = Mock()
            FunkovConnection funkovConnection = new FunkovConnection(new MockSocketChannel(false, false), new MockKey(), connectionListener)
        when:
            funkovConnection.run()
            funkovConnection.close()
        then:
            funkovConnection.associatedByteBuffer() == null
    }
    
    
    private static class MockSocketChannel extends SocketChannel
    {
        private final boolean throwOnRead
        private final boolean throwOnWrite
        
        protected MockSocketChannel(boolean throwOnRead, boolean throwOnWrite)
        {
            super(null)
            this.throwOnRead = throwOnRead
            this.throwOnWrite = throwOnWrite
        }
    
        @Override
        protected void implCloseSelectableChannel() throws IOException {
        
        }
    
        @Override
        protected void implConfigureBlocking(boolean block) throws IOException {
        
        }
    
        @Override
        SocketChannel bind(SocketAddress local) throws IOException
        {
            return null
        }
    
        @Override
        <T> SocketChannel setOption(SocketOption<T> name, T value) throws IOException {
            return null
        }
    
        @Override
        <T> T getOption(SocketOption<T> name) throws IOException {
            return null
        }
    
        @Override
        Set<SocketOption<?>> supportedOptions() {
            return null
        }
    
        @Override
        SocketChannel shutdownInput() throws IOException {
            return null
        }
    
        @Override
        SocketChannel shutdownOutput() throws IOException {
            return null
        }
    
        @Override
        Socket socket() {
            return null
        }
    
        @Override
        boolean isConnected() {
            return false
        }
    
        @Override
        boolean isConnectionPending() {
            return false
        }
    
        @Override
        boolean connect(SocketAddress remote) throws IOException {
            return false
        }
    
        @Override
        boolean finishConnect() throws IOException {
            return false
        }
    
        @Override
        SocketAddress getRemoteAddress() throws IOException {
            return null
        }
    
        @Override
        int read(ByteBuffer dst) throws IOException
        {
            if(throwOnRead)
                throw new TestExpectedException()
            return 0
        }
    
        @Override
        long read(ByteBuffer[] dsts, int offset, int length) throws IOException
        {
            return 0
        }
    
        @Override
        int write(ByteBuffer src) throws IOException
        {
            if(throwOnWrite)
                throw new TestExpectedException()
            return 0
        }
    
        @Override
        long write(ByteBuffer[] srcs, int offset, int length) throws IOException
        {
            return 0
        }
    
        @Override
        SocketAddress getLocalAddress() throws IOException {
            return null
        }
    }
    private static class MockKey extends SelectionKey
    {
    
        @Override
        SelectableChannel channel()
        {
            return null
        }
    
        @Override
        Selector selector() {
            return new Selector() {
                @Override
                boolean isOpen() {
                    return false
                }
    
                @Override
                SelectorProvider provider() {
                    return null
                }
    
                @Override
                Set<SelectionKey> keys() {
                    return Collections.emptySet()
                }
    
                @Override
                Set<SelectionKey> selectedKeys() {
                    return Collections.emptySet()
                }
    
                @Override
                int selectNow() throws IOException {
                    return 0
                }
    
                @Override
                int select(long timeout) throws IOException {
                    return 0
                }
    
                @Override
                int select() throws IOException {
                    return 0
                }
    
                @Override
                Selector wakeup() {
                    return null
                }
    
                @Override
                void close() throws IOException {
        
                }
            }
        }
    
        @Override
        boolean isValid() {
            return false
        }
    
        @Override
        void cancel() {
        
        }
    
        @Override
        int interestOps() {
            return 0
        }
    
        @Override
        SelectionKey interestOps(int ops) {
            return null
        }
    
        @Override
        int readyOps() {
            return 0
        }
    }
}
