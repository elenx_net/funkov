package net.elenx.funkov.server.reactor

import net.elenx.funkov.common.handler.BooleanHandler
import net.elenx.funkov.common.handler.Handlers
import net.elenx.funkov.server.Server
import net.elenx.funkov.server.configuration.FunkovServerConfiguration
import net.elenx.funkov.serverapi.ServerListener
import net.elenx.funkov.serverapi.mock.MockServerListener
import net.elenx.funkov.serverapi.mock.Warmup
import spock.lang.Specification

class ServerListenerTest extends Specification
{
    private static final port = 5555
    private static final FunkovServerConfiguration CONFIGURATION = FunkovServerConfiguration.builder()
            .port(port)
            .build()
    
    def "should invoke onStartServer() method when server is started"()
    {
        given:
            BooleanHandler handler = Handlers.handler()
            ServerListener listener = MockServerListener.builder().onStartServer(() -> handler.invoke()).build()
        when:
            Server server = Server.server(CONFIGURATION, listener)
            server.startOnNewThread()
        then:
            handler.invoked()
        cleanup:
            server.close()
    }
    
    def "should invoke onCloseServer() method on closing server"()
    {
        given:
            BooleanHandler handler = Handlers.handler()
            ServerListener listener = MockServerListener.builder().onCloseServer(() -> handler.invoke()).build()
        when:
            Server server = Server.server(CONFIGURATION, listener)
            server.startOnNewThread()
        then:
            server.close()
            handler.invoked()
    }
    
    def "should invoke onConnectionAccept() method on incoming connection"()
    {
        given:
            BooleanHandler handler = Handlers.handler()
            ServerListener listener = MockServerListener.builder().onConnectionAccept({
                handler.invoke()
                Optional.empty()
            }).build()
        when:
            Server server = Server.server(CONFIGURATION, listener)
            server.startOnNewThread()
            Socket clientSocket = new Socket("127.0.0.1", port)
        then:
            handler.invoked()
        cleanup:
            server.close()
            clientSocket.close()
    }
    
    def "should invoke afterWarmup() method after warmup"()
    {
        given:
            BooleanHandler handler = Handlers.handler()
            
            ServerListener listener = MockServerListener.builder()
                    .afterWarmup(() -> handler.invoke())
                    .build()
            
            FunkovServerConfiguration warmupConfiguration = FunkovServerConfiguration.builder()
                    .port(port)
                    .warmup(new TestWarmup())
                    .build()
        when:
            Server server = Server.server(warmupConfiguration, listener)
            server.startOnNewThread()
        then:
            handler.invoked()
        cleanup:
            server.close()
    }
    
    private static class TestWarmup implements Warmup
    {
        
        @Override
        int warmupServerPort()
        {
            return 5556
        }
        
        @Override
        void doWarmup()
        {
        }
    }
}
