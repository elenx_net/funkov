package net.elenx.funkov.model;

import java.util.Arrays;

public class MutableHandshake implements Handshake
{
    private static final String WEB_SOCKET_GUID = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";

    private final String[] handshakeData = new String[5];
    private boolean accepted;
    private final byte[] webSocketKey = new byte[60];

    public MutableHandshake()
    {
        // 23 because we need space for client Sec-WebSocket-Key header which takes 24 bytes
        System.arraycopy(WEB_SOCKET_GUID.getBytes(), 0, webSocketKey, 24, WEB_SOCKET_GUID.length());
    }

    public MutableHandshake(String resourceUri, String host, String httpVersion, String origin, String subProtocols)
    {
        handshakeData[0] = resourceUri;
        handshakeData[1] = host;
        handshakeData[2] = httpVersion;
        handshakeData[3] = origin;
        handshakeData[4] = subProtocols;
    }

    @Override
    public String resourceUri()
    {
        return handshakeData[0];
    }

    @Override
    public String host()
    {
        return handshakeData[1];
    }

    @Override
    public String httpVersion()
    {
        return handshakeData[2];
    }

    @Override
    public String origin()
    {
        return handshakeData[3];
    }

    @Override
    public String subProtocols()
    {
        return handshakeData[4];
    }

    @Override
    public Handshake copyForStore()
    {
        return new MutableHandshake(handshakeData[0], handshakeData[1], handshakeData[2], handshakeData[3], handshakeData[4]);
    }

    public void setResourceUri(String resourceUri)
    {
        this.handshakeData[0] = resourceUri;
    }

    public void setHost(String host)
    {
        this.handshakeData[1] = host;
    }

    public void setHttpVersion(String httpVersion)
    {
        this.handshakeData[2] = httpVersion;
    }

    public void setOrigin(String origin)
    {
        this.handshakeData[3] = origin;
    }

    public void setSubProtocols(String subProtocols)
    {
        this.handshakeData[4] = subProtocols;
    }

    public byte[] getWebsocketKey()
    {
        return webSocketKey;
    }

    public boolean isAccepted()
    {
        return accepted;
    }

    public MutableHandshake accept()
    {
        return accept(true);
    }

    public MutableHandshake revoke()
    {
        return accept(false);
    }

    private MutableHandshake accept(boolean accepted)
    {
        this.accepted = accepted;
        return this;
    }

    public void resetToNulls()
    {
        handshakeData[0] = null;
        handshakeData[1] = null;
        handshakeData[2] = null;
        handshakeData[3] = null;
        handshakeData[4] = null;
    }

    @Override
    public String toString()
    {
        return "MutableHandshake{" +
                "handshakeData=" + Arrays.toString(handshakeData) +
                ", accepted=" + accepted +
                '}';
    }
}
