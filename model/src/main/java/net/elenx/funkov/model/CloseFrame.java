package net.elenx.funkov.model;

public class CloseFrame
{
    private static final byte OP_CODE = 8;
    public static final short CODE_NORMAL_CLOSE = 1000;
    public static final short CODE_ENDPOINT_GOING_AWAY = 1001;
    public static final short CODE_PROTOCOL_ERROR = 1002;
    public static final short CODE_UNSUPPORTED_DATA_TYPE = 1003;
    public static final short CODE_EXPECTED_STATUS_CODE_NOT_PRESENT = 1005;
    public static final short CODE_CLOSED_ABNORMALLY = 1006;
    public static final short CODE_INVALID_TEXT_FRAME_ENCODING = 1007;
    public static final short CODE_BAD_MESSAGE = 1008;
    public static final short CODE_TOO_BIG_MESSAGE = 1009;


    public static final short CODE_CANNOT_NEGOTIATE_EXTENSION = 1010;
    public static final short CODE_UNEXPECTED_CONDITION = 1011;
    public static final short CODE_FAILED_TLS_HANDSHAKE = 1011;


    private final short code;
    private final String reason;
    private final boolean rsv1;
    private final boolean rsv2;
    private final boolean rsv3;

    public static final CloseFrame CLOSE_FRAME = new CloseFrame();

    public CloseFrame()
    {
        this(CODE_NORMAL_CLOSE, "", false, false, false);
    }

    public CloseFrame(short code, String reason, boolean rsv1, boolean rsv2, boolean rsv3)
    {
        this.code = code;
        this.reason = reason;
        this.rsv1 = rsv1;
        this.rsv2 = rsv2;
        this.rsv3 = rsv3;
    }

    public short getCode()
    {
        return code;
    }

    public String getReason()
    {
        return reason;
    }

    public boolean isRsv1()
    {
        return rsv1;
    }

    public boolean isRsv2()
    {
        return rsv2;
    }

    public boolean isRsv3()
    {
        return rsv3;
    }

    public byte getOpCode()
    {
        return OP_CODE;
    }
}
