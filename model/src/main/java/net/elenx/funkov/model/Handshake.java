package net.elenx.funkov.model;

public interface Handshake
{
    /**
     * @return URI requested by client.
     * example: ws://funkov.example.com:12010/updates
     */
    String resourceUri();

    /**
     * @return host which is part of {@link #resourceUri()}.
     * example: funkov.example.com
     */
    String host();

    /**
     * @return HTTP version which was used for handshake. At least 1.1 version.
     */
    String httpVersion();

    /**
     * @return Origin header field if handshake was requested by browser, null otherwise.
     */
    String origin();

    /**
     * @return Comma separated subprotocols taken from client's Sec-WebSocket-Protocol http header, null otherwise.
     * example: "soap, wamp"
     */
    String subProtocols();

    /**
     * @return {@link Handshake} object ready for store. You should use this method if you are going to keep {@link Handshake} objects somewhere
     * instead of storing <code>this</code> instance.
     * <p>
     * Handshake objects are reused to keep GC footprint as small as possible, this method just creates new immutable instance.
     */
    Handshake copyForStore();
}
