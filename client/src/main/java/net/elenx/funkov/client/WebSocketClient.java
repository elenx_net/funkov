package net.elenx.funkov.client;


public interface WebSocketClient
{
    void connect(WebSocketClientListener webSocketClientListener);
    void close();
}
