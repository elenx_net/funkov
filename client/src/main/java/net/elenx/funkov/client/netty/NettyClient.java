package net.elenx.funkov.client.netty;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.HttpClientCodec;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.websocketx.WebSocketClientHandshakerFactory;
import io.netty.handler.codec.http.websocketx.WebSocketVersion;
import net.elenx.funkov.client.WebSocketClient;
import net.elenx.funkov.client.WebSocketClientListener;

import java.net.InetSocketAddress;
import java.net.URI;

import static net.elenx.funkov.common.SneakyThrow.sneakyThrow;

public class NettyClient implements WebSocketClient
{
    private final URI uri;
    private final InetSocketAddress serverAddress;
    private final EventLoopGroup group = new NioEventLoopGroup();

    public NettyClient(String uri)
    {
        this.uri = URI.create(uri);
        this.serverAddress = new InetSocketAddress(this.uri.getHost(), this.uri.getPort());
    }

    @Override
    public void connect(WebSocketClientListener webSocketClientListener)
    {
        Bootstrap b = new Bootstrap();
        String protocol = uri.getScheme();
        if (!"ws".equals(protocol))
        {
            throw new IllegalArgumentException("Unsupported protocol: " + protocol);
        }

        // Connect with V13 (RFC 6455 aka HyBi-17). You can change it to V08 or V00.
        // If you change it to V00, ping is not supported and remember to change
        // HttpResponseDecoder to WebSocketHttpResponseDecoder in the pipeline.
        final WebSocketClientHandler handler =
                new WebSocketClientHandler(
                        webSocketClientListener,
                        WebSocketClientHandshakerFactory.newHandshaker(
                                uri, WebSocketVersion.V13, null, false, HttpHeaders.EMPTY_HEADERS, 1280000));

        b.group(group)
                .channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<SocketChannel>()
                {
                    @Override
                    public void initChannel(SocketChannel ch)
                    {
                        ChannelPipeline pipeline = ch.pipeline();
                        pipeline.addLast("http-codec", new HttpClientCodec());
                        pipeline.addLast("aggregator", new HttpObjectAggregator(65536));
                        pipeline.addLast("ws-handler", handler);
                    }
                });

        try
        {
            b.connect(serverAddress).sync().channel();
            handler.handshakeFuture();
        } catch (Exception e)
        {
            sneakyThrow(e);
        }
    }

    @Override
    public void close()
    {
        group.shutdownGracefully();
        while (!group.isShutdown()) ;
    }
}
