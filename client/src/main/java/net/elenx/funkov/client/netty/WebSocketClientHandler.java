package net.elenx.funkov.client.netty;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.websocketx.BinaryWebSocketFrame;
import io.netty.handler.codec.http.websocketx.CloseWebSocketFrame;
import io.netty.handler.codec.http.websocketx.PongWebSocketFrame;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketClientHandshaker;
import io.netty.handler.codec.http.websocketx.WebSocketFrame;
import io.netty.util.CharsetUtil;
import net.elenx.funkov.client.WebSocketClientListener;
import net.elenx.funkov.model.CloseFrame;
import net.elenx.funkov.model.MutableHandshake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;

public class WebSocketClientHandler extends SimpleChannelInboundHandler<Object>
{
    private static final Logger logger = LoggerFactory.getLogger(WebSocketClientHandler.class);

    private final WebSocketClientHandshaker handshaker;
    private final WebSocketClientListener webSocketClientListener;
    private ChannelPromise handshakeFuture;

    public WebSocketClientHandler(WebSocketClientListener webSocketClientListener, final WebSocketClientHandshaker handshaker)
    {
        this.handshaker = handshaker;
        this.webSocketClientListener = webSocketClientListener;
    }

    public ChannelFuture handshakeFuture()
    {
        return handshakeFuture;
    }

    @Override
    public void handlerAdded(final ChannelHandlerContext ctx)
    {
        handshakeFuture = ctx.newPromise();
    }

    @Override
    public void channelActive(final ChannelHandlerContext ctx)
    {
        handshaker.handshake(ctx.channel());
    }

    @Override
    public void channelInactive(final ChannelHandlerContext ctx)
    {
        logger.info("WebSocket Client disconnected!");
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception
    {
        final Channel ch = ctx.channel();
        if (!handshaker.isHandshakeComplete())
        {
            // web socket client connected
            FullHttpResponse response = (FullHttpResponse) msg;
            handshaker.finishHandshake(ch, response);
            handshakeFuture.setSuccess();
            String host = ((InetSocketAddress) ctx.channel().remoteAddress()).getAddress().getHostAddress();
            MutableHandshake mutableHandshake = new MutableHandshake();
            webSocketClientListener.onConnect(mutableHandshake.accept());
            return;
        }

        if (msg instanceof FullHttpResponse)
        {
            final FullHttpResponse response = (FullHttpResponse) msg;
            throw new Exception("Unexpected FullHttpResponse (getStatus=" + response.getStatus() + ", content="
                    + response.content().toString(CharsetUtil.UTF_8) + ')');
        }

        final WebSocketFrame frame = (WebSocketFrame) msg;
        if (frame instanceof TextWebSocketFrame)
        {
            final TextWebSocketFrame textFrame = (TextWebSocketFrame) frame;
            // uncomment to print request
            // logger.info(textFrame.text());
        } else if (frame instanceof PongWebSocketFrame)
        {
        } else if (frame instanceof CloseWebSocketFrame closeFrame)
        {
            String reason = closeFrame.reasonText();
            logger.info("Received close frame code {}, reason: {}", closeFrame.statusCode(), reason);
            webSocketClientListener.onCloseFrame(CloseFrame.CLOSE_FRAME);
            ch.close();
        }
        else if (frame instanceof BinaryWebSocketFrame)
        {
            // uncomment to print request
            // logger.info(frame.content().toString());
        }

    }

    @Override
    public void exceptionCaught(final ChannelHandlerContext ctx, final Throwable cause) throws Exception
    {
        cause.printStackTrace();

        if (!handshakeFuture.isDone())
        {
            handshakeFuture.setFailure(cause);
        }

        ctx.close();
    }
}
