package net.elenx.funkov.client;

import net.elenx.funkov.model.CloseFrame;
import net.elenx.funkov.model.Handshake;

public interface WebSocketClientListener
{
    default void onConnect(Handshake handshake) {}
    default void onCloseFrame(CloseFrame closeFrame) {}
    default void onClose() {}
}
