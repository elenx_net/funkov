package net.elenx.funkov.client;

import net.elenx.funkov.client.netty.NettyClient;

public class WebSocketClientFactory
{
    public static WebSocketClient nettyClient(String uri)
    {
        return new NettyClient(uri);
    }
}
