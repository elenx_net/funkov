package net.elenx.funkov.funkovbenchmark;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public record BenchmarkProgramArguments(String target, int connections, int requests, boolean keepAlive)
{

    public static BenchmarkProgramArguments initialize(String... args)
    {
        var options = new Options();
        options.addOption(Option.builder("t")
                .longOpt("target")
                .hasArg(true)
                .desc("websocket address")
                .required(true)
                .build());

        options.addOption(Option.builder("c")
                .longOpt("connections")
                .hasArg(true)
                .desc("concurrent connections")
                .required(true)
                .build());

        options.addOption(Option.builder("n")
                .longOpt("requests")
                .hasArg(true)
                .desc("how many requests to run")
                .required(true)
                .build());

        options.addOption(Option.builder("k")
                .longOpt("keep-alive")
                .hasArg(false)
                .desc("keep-alive")
                .required(false)
                .build());

        var parser = new DefaultParser();
        CommandLine cmd;
        try
        {
            cmd = parser.parse(options, args);

            return new BenchmarkProgramArguments(
                    cmd.getOptionValue("t"),
                    Integer.parseInt(cmd.getOptionValue("c")),
                    Integer.parseInt(cmd.getOptionValue("n")),
                    Boolean.parseBoolean(cmd.getOptionValue("k"))
            );
        } catch (ParseException | NumberFormatException exc)
        {
            System.out.println("Error parsing command-line arguments!");
            var formatter = new HelpFormatter();
            formatter.printHelp("java -jar --enable-preview client-$version.jar -t websocketaddress -c 4 -n 20 [-k]", options);
            System.exit(1);
            return null;
        }
    }
}
