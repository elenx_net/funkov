package net.elenx.funkov.funkovbenchmark;

import net.elenx.funkov.client.WebSocketClientFactory;
import net.elenx.funkov.client.WebSocketClientListener;
import net.elenx.funkov.model.Handshake;

import java.util.concurrent.CompletableFuture;

public class Main
{
    public static void main(String[] args)
    {
        disableIllegalReflectiveAccessWarning();
        var programArguments = BenchmarkProgramArguments.initialize(args);

        var client = WebSocketClientFactory.nettyClient(programArguments.target());
        for (var i = 0; i < programArguments.connections(); i++)
        {
            CompletableFuture<Void> onHandshake = new CompletableFuture<>();
            client.connect(createListener(onHandshake));
        }

        if(!programArguments.keepAlive())
            client.close();
    }

    private static WebSocketClientListener createListener(CompletableFuture<Void> onHandshake)
    {
        return new WebSocketClientListener()
        {
            @Override
            public void onConnect(Handshake handshake)
            {
                onHandshake.complete(null);
            }
        };
    }

    // https://stackoverflow.com/questions/46454995/how-to-hide-warning-illegal-reflective-access-in-java-9-without-jvm-argument
    public static void disableIllegalReflectiveAccessWarning() {
        System.err.close();
        System.setErr(System.out);
    }
}
