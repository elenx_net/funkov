## Funkov  
Websocket server and client designed to handle massive connection numbers with the lowest possible latency.  

**Funkov** architecture is based on some variation of Reactor pattern, similary to Netty framework.  

![Reactor pattern](<.images/reactor_pattern.png>)

Websocket protocol description: https://tools.ietf.org/html/rfc6455
## Requirements: 
- JDK 15  
- Run maven antrun:run task in `integrationtests` module which creates your own local configuration file. Then fill properties according to your local environment

## Roadmap:
### Milestone 1
First milestone is initial setup, including libraries, test frameworks, CI/CD, working out good practices and conventions. 
Initial version of `Reactor` is also neccessary as background for testing and futher development. Main goals are:  
- Easy access to configuration properties defined in files and system properties.  
- Server is capable to handle 10k concurrent "handshake" connections  
### Milestone 2
Second milestone is focused mainly around providing full (without extensions) Websocket protocol implementation on the server side and initial Websocket client setup. Goals:  
- Server replies for websocket requests (continuation, text, binary, close, ping and pong frames)  
- There is general idea about Websocket Client architecture
- Servier is capable to handle 10k concurrent websocket connections  
### Milestone 3
Pump server to handle 50k concurrent websocket connections, a lot of profiling here. We should also get the working Websocket client. Goals:  
- Server is capable to handle 50k concurrent websocket connections
- Working basic (without extensions) Websocket client.  
### Futher milestones...
- Streaming extension - server and client know how to stream data according to websocket protocol  
- Subscriber - the most common and desirable feature for websocket servers which allows to instantly push (or just forward) data  incoming from external services to clients  
- Publisher - feature which allows pubslihing data to all connected clients. It would be implemented using Subscriber feature
- handling 100k connections (or more)
- Funkov in cluster
- and more...


Make sure setup process went well by running ServerRunner class, it should start server and execute some warmup iterations.
